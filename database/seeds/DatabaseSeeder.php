<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\App::setLocale('hu');
        $this->call(UsersTableSeeder::class);
        $this->call(\Modules\Admin\Database\Seeders\AdminDatabaseSeeder::class);
        $this->call(\Modules\Blog\Database\Seeders\BlogDatabaseSeeder::class);
        $this->call(\Modules\Slider\Database\Seeders\SliderDatabaseSeeder::class);
        $this->call(\Modules\Category\Database\Seeders\CategoryDatabaseSeeder::class);
        $this->call(\Modules\Menu\Database\Seeders\MenuDatabaseSeeder::class);
        $this->call(\Modules\ModuleBuilder\Database\Seeders\ModuleBuilderDatabaseSeeder::class);
        $this->call(\Modules\Page\Database\Seeders\PageDatabaseSeeder::class);
        $this->call(\Modules\Role\Database\Seeders\RoleDatabaseSeeder::class);
        $this->call(\Modules\System\Database\Seeders\SystemDatabaseSeeder::class);
        $this->call(\Modules\Translation\Database\Seeders\TranslationDatabaseSeeder::class);
        $this->call(\Modules\User\Database\Seeders\UserDatabaseSeeder::class);
       // $this->call(\Modules\WebshopFrontend\Database\Seeders\WebshopFrontendDatabaseSeeder::class);
       // $this->call(\Modules\WebshopFrontend\Database\Seeders\MenusTableSeeder::class);
      //  $this->call(\Modules\WebshopFrontend\Database\Seeders\MenuItemsTableSeeder::class);

$this->call(\Modules\Galery\Database\Seeders\GaleryDatabaseSeeder::class);

$this->call(\Modules\Galery\Database\Seeders\GaleryDatabaseSeeder::class);

$this->call(\Modules\Galery\Database\Seeders\GaleryDatabaseSeeder::class);

$this->call(\Modules\GaleryImages\Database\Seeders\GaleryImagesDatabaseSeeder::class);

$this->call(\Modules\Galery\Database\Seeders\GaleryDatabaseSeeder::class);

$this->call(\Modules\GaleryImages\Database\Seeders\GaleryImagesDatabaseSeeder::class);

$this->call(\Modules\Member\Database\Seeders\MemberDatabaseSeeder::class);

$this->call(\Modules\Partners\Database\Seeders\PartnersDatabaseSeeder::class);

$this->call(\Modules\Review\Database\Seeders\ReviewDatabaseSeeder::class);

$this->call(\Modules\Topblock\Database\Seeders\TopblockDatabaseSeeder::class);

/* ModuleBuilderSeedArea *//* ModuleBuilderSeedAreaEnd */

        if (config('app.env') == 'local') {
            \Modules\User\Entities\User::unsetEventDispatcher();
            \Modules\Role\Entities\Role::unsetEventDispatcher();
            \Modules\Blog\Entities\Blog::unsetEventDispatcher();
            \Modules\Page\Entities\Page::unsetEventDispatcher();
            \Modules\Color\Entities\Color::unsetEventDispatcher();
            \Modules\Pack\Entities\Pack::unsetEventDispatcher();
            \Modules\Type\Entities\Type::unsetEventDispatcher();
            \Modules\Product\Entities\Product::unsetEventDispatcher();
            \Modules\ProductVariation\Entities\ProductVariation::unsetEventDispatcher();
            \Modules\ProductCategory\Entities\ProductCategory::unsetEventDispatcher();

\Modules\OrderItem\Entities\OrderItem::unsetEventDispatcher();

\Modules\Banner\Entities\Banner::unsetEventDispatcher();

\Modules\Order\Entities\Order::unsetEventDispatcher();

\Modules\Order\Entities\Order::unsetEventDispatcher();

\Modules\Coupon\Entities\Coupon::unsetEventDispatcher();

\Modules\People\Entities\People::unsetEventDispatcher();

\Modules\Event\Entities\Event::unsetEventDispatcher();

\Modules\Members\Entities\Members::unsetEventDispatcher();

\Modules\Galery\Entities\Galery::unsetEventDispatcher();

\Modules\Galery\Entities\Galery::unsetEventDispatcher();

\Modules\Galery\Entities\Galery::unsetEventDispatcher();

\Modules\GaleryImages\Entities\GaleryImages::unsetEventDispatcher();

\Modules\Galery\Entities\Galery::unsetEventDispatcher();

\Modules\GaleryImages\Entities\GaleryImages::unsetEventDispatcher();

\Modules\Member\Entities\Member::unsetEventDispatcher();

\Modules\Partners\Entities\Partners::unsetEventDispatcher();

\Modules\Review\Entities\Review::unsetEventDispatcher();

\Modules\Topblock\Entities\Topblock::unsetEventDispatcher();

/* EventDispatcherArea *//* EventDispatcherAreaEnd */

            $user = factory(\Modules\User\Entities\User::class, 20)->create()
                ->each(function ($user) {
                    $user->shipping()->save(factory(\Modules\User\Entities\UserShipping::class)->make([
                        'user_id' => $user->id
                    ]));

                    $user->billing()->save(factory(\Modules\User\Entities\UserBilling::class)->make([
                        'user_id' => $user->id
                    ]));

    });

            $role = factory(\Modules\Role\Entities\Role::class, 8)->create();

            $blog = factory(\Modules\Blog\Entities\Blog::class, 20)->create()
                ->each(function ($b) {
                    $b->categories()->save(factory(\Modules\Category\Entities\Category::class)->make());
                    $b->tags()->save(factory(\Modules\Category\Entities\Tag::class)->make());
                });

            $model = factory(\Modules\Color\Entities\Color::class, 5)->create();
            $model = factory(\Modules\Slider\Entities\Slider::class, 5)->create();

            $model = factory(\Modules\Pack\Entities\Pack::class, 5)->create();

            $model = factory(\Modules\Type\Entities\Type::class, 5)->create();
            $model = factory(\Modules\ProductCategory\Entities\ProductCategory::class, 20)->create();
            foreach (\Modules\ProductCategory\Entities\ProductCategory::limit(10)->offset(10)->get()  as $c){
                $c->parent_id = rand(1,10);
                $c->save();
            }

            $model = factory(\Modules\Product\Entities\Product::class, 300)->create();
            foreach (\Modules\Product\Entities\Product::all() as $p){
                $p->categories()->sync(rand(1,20));
            }
            $model = factory(\Modules\ProductQuestion\Entities\ProductQuestion::class, 300)->create();
            $model = factory(\Modules\ProductRating\Entities\ProductRating::class, 300)->create();

            $model = factory(\Modules\ProductVariation\Entities\ProductVariation::class, 900)->create();
            foreach (\Modules\ProductVariation\Entities\ProductVariation::all() as $v){
                for($i = 0; $i< 5;$i++) {
                    $k = new \Modules\ProductVariation\Entities\ProductVariationImage();
                    $k->variation_id = $v->id;
                    $k->url = 'https://source.unsplash.com/800x800/?salt&'.rand(0,9999);
                    $k->save();
                }
            }


            /*$model = factory(\Modules\OrderItem\Entities\OrderItem::class, 5)->create();*/

            $model = factory(\Modules\Banner\Entities\Banner::class, 6)->create();

$model = factory(\Modules\Order\Entities\Order::class, 5)->create();

$model = factory(\Modules\Order\Entities\Order::class, 5)->create();

$model = factory(\Modules\Coupon\Entities\Coupon::class, 5)->create();

$model = factory(\Modules\People\Entities\People::class, 5)->create();

$model = factory(\Modules\Event\Entities\Event::class, 5)->create();

$model = factory(\Modules\Members\Entities\Members::class, 5)->create();

$model = factory(\Modules\Galery\Entities\Galery::class, 5)->create();

$model = factory(\Modules\Galery\Entities\Galery::class, 5)->create();

$model = factory(\Modules\Galery\Entities\Galery::class, 5)->create();

$model = factory(\Modules\GaleryImages\Entities\GaleryImages::class, 5)->create();

$model = factory(\Modules\Galery\Entities\Galery::class, 5)->create();

$model = factory(\Modules\GaleryImages\Entities\GaleryImages::class, 5)->create();

$model = factory(\Modules\Member\Entities\Member::class, 5)->create();

$model = factory(\Modules\Partners\Entities\Partners::class, 5)->create();

$model = factory(\Modules\Review\Entities\Review::class, 5)->create();

$model = factory(\Modules\Topblock\Entities\Topblock::class, 5)->create();

/* FakerArea *//* FakerAreaEnd */

        }
    }
}

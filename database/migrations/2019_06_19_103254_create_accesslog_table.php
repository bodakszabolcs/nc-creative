<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccesslogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);

        Schema::create('accesslogs', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->comment('Felhasználó')->unsigned();
            $table->string('route')->comment('Route');
            $table->string('action')->comment('Művelet');
            $table->longText('extra')->comment('Extra info');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['user_id','route']);
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accesslogs');
    }
}

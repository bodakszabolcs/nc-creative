<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\Webshop\Entities\Order;
use Modules\Webshop\Entities\OrderStatus;
use Modules\Webshop\Entities\OrderStatusChangelog;

class StatusChangeEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    public function __construct(\Modules\Order\Entities\Order $order)
    {
        $this->order = $order;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $email =$this->markdown('mail.status')->locale($this->order->locale)->subject(__('Order email - ').'#'.$this->order->id.' - '. config('app.name'));
    }
}

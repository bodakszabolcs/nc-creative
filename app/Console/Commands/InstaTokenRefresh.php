<?php

	namespace App\Console\Commands;
	use App\Http\Middleware\AccessLog;
	use Faker\Provider\Image;
	use Illuminate\Console\Command;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\File;
	use Symfony\Component\Process\Exception\ProcessFailedException;
	use Symfony\Component\Process\Process;
	use ImageOptimizer;
	class InstaTokenRefresh extends Command
	{
		/**
		 * The name and signature of the console command.
		 *
		 * @var string
		 */
		protected $signature = 'instaToken:refresh';
		/**
		 * The console command description.
		 *
		 * @var string
		 */
		protected $description = 'refresh instaToken';
		protected $process;

        private $refreshUrl ='https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token=';

		/**
		 * Create a new command instance.
		 *
		 * @return void
		 */
		public function __construct()
		{
			parent::__construct();

		}

		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		public function handle()
		{
            $token = config('app.instaToken');
			if(file_exists(public_path('insta.json'))){
                $content = json_decode(file_get_contents(public_path('insta.json')),1);
                if(isset($content['access_token'])) {
                    $token = $content['access_token'];
                }
            }
            $contents = file_get_contents($this->refreshUrl.$token);

            file_put_contents(public_path('insta.json'),$contents);
		}

	}

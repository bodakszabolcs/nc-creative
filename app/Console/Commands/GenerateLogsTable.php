<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GenerateLogsTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:logs_table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate logs table for accesslog and maillog.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date("Y-m-d", strtotime("+ 2 weeks"));

        $accessLogTable = 'accesslogs_'.date("Y", strtotime($date)).'_'.date("m", strtotime($date));
        $mailLogTable = 'maillogs_'.date("Y", strtotime($date)).'_'.date("m", strtotime($date));

        if (!Schema::hasTable($accessLogTable)) {
            Schema::defaultStringLength(191);

            Schema::create($accessLogTable, function (Blueprint $table) {
                $table->increments('id');
                $table->bigInteger('user_id')->unsigned()->comment('Felhasználó');
                $table->string('route')->comment('Route');
                $table->string('action')->comment('Művelet');
                $table->longText('extra')->comment('Extra info');
                $table->timestamps();
                $table->softDeletes();
                $table->index(['user_id','route']);
                $table->foreign('user_id')->references('id')->on('users');
            });
        }

        if (!Schema::hasTable($mailLogTable)) {
            Schema::defaultStringLength(191);

            Schema::create($mailLogTable, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('to')->nullable();
                $table->string('subject')->nullable();
                $table->longText('message')->nullable();
                $table->longText('extra')->nullable();
                $table->timestamps();
                $table->index(['to']);
            });
        }
    }
}

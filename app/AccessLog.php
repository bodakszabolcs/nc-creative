<?php

namespace App;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Schema;

class AccessLog extends BaseModel
{
    use SoftDeletes, Cachable;

    protected $table = 'accesslogs';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'route',
        'action',
        'extra'

    ];

    public function __construct(array $attributes = [])
    {
        if (Schema::hasTable('accesslogs_'.date("Y").'_'.date("m"))) {
            $this->table = 'accesslogs_'.date("Y").'_'.date("m");
        }

        parent::__construct($attributes);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'extra' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo('Modules\User\Entities\User', 'user_id', 'id');
    }
}

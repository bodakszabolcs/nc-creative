<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;

abstract class ModuleServiceProvider extends ServiceProvider
{
    protected $module;
    protected $directory;
    protected $isRegisterViews = false;
    protected $isRegisterTranslations = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom($this->directory . '/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            $this->directory . '/../Config/config.php' => config_path($this->module . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            $this->directory . '/../Config/config.php', $this->module
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        if ($this->isRegisterViews) {
            $viewPath = resource_path('views/modules/' . $this->module);

            $sourcePath = $this->directory . '/../Resources/views';

            $this->publishes([
                $sourcePath => $viewPath
            ], 'views');

            $this->loadViewsFrom(array_merge(\Config::get('view.paths'), [$sourcePath]), $this->module);
        }
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        if ($this->isRegisterTranslations) {
            $langPath = resource_path('lang/modules/' . $this->module);

            if (is_dir($langPath)) {
                $this->loadTranslationsFrom($langPath, $this->module);
            } else {
                $this->loadTranslationsFrom($this->directory . '/../Resources/lang', $this->module);
            }
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load($this->directory . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}

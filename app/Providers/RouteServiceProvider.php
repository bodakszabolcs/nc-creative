<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Nwidart\Modules\Facades\Module;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    private $excludedRoutes = [
        'PayBill',
        'Admin',
        'System',
        'Translation',
        'Webshop'
    ];

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        if (config('paybill.simplepay_enabled')) {
            $this->loadRoutesFrom(__DIR__ . '/../../Modules/PayBill/Routes/simplepay.php');
        }

        Route::group([
            'middleware' => 'api',
            'prefix' => 'api/v1',
        ], function () {
            foreach (Module::allEnabled() as $module) {
                if (!in_array($module->getName(), $this->excludedRoutes)) {
                    Route::group([
                        'namespace' => "Modules\\" . $module->getName() . "\Http\Controllers",
                        'prefix' => '/'.Str::slug($module->getName()),
                        'middleware' => ['auth:sanctum', 'role', 'accesslog']
                    ], function () use ($module) {
                        Route::get('/index', $module->getName() . 'Controller@index')->name($module->getName() . ' list');
                        Route::get('/show/{id?}', $module->getName() . 'Controller@show')->name($module->getName() . ' show');
                        Route::post('/create', $module->getName() . 'Controller@create')->name($module->getName() . ' create');
                        Route::put('/update/{id}', $module->getName() . 'Controller@update')->name($module->getName() . ' update');
                        Route::delete('/delete/{id}', $module->getName() . 'Controller@destroy')->name($module->getName() . ' delete');
                        Route::delete('/bulk-delete', $module->getName() . 'Controller@destroyBulk')->name($module->getName() . ' bulk delete');
                    });
                }
                require($module->getPath() . '/Routes/api.php');
            }

            require base_path('routes/api.php');

            Route::any('/{any?}', 'App\Http\Controllers\Controller@_404')->where('any', '.*');
        });
    }
}

<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;

/**
 * Class PreventSendMessage
 * Disable email sending in non production environment.
 * @package App\Listeners
 */
class PreventSendMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        /*if (config('app.env') !== 'production' && !in_array($event->data['user']->email,config('mail.allowed_addresses'))) {
            Log::info('Outgoing email disabled for:'.$event->data['user']->email);
            return false;
        }*/
    }
}

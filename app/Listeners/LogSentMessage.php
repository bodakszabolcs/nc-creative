<?php

namespace App\Listeners;

use App\MailLog;

/**
 * Class LogSentMessage
 * Log sent emails to database.
 * @package App\Listeners
 */
class LogSentMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $ml = new MailLog();
        $ml->to = (isset($event->data['user']->email)) ? $event->data['user']->email : '-';
        $ml->subject = $event->message->getSubject();
        $ml->message = $event->message->getBody();
        $ml->extra = (isset($event->data['extra']) && is_array($event->data['extra'])) ? json_encode($event->data['extra']) : json_encode([]);
        $ml->save();
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\System\Entities\Settings;

class BaseResource extends JsonResource
{
    protected function getTranslatable($field) {
        $settings = Settings::where('settings_key','=','language')->first();
        $lang = json_decode($settings->settings_value, true);
        foreach ($lang as $key => $val) {
            $lang[$key] = null;
        }

        $trans = $this->getTranslations($field);
        if (count($trans) < $lang) {
            $trans = array_merge($lang, $trans);
        }

        return $trans;
    }
}

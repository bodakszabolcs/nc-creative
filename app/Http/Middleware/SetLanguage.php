<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Modules\Webshop\Entities\Currency;

class SetLanguage
{
    /**
     * Setting up App langauge from Language header parameter.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('Language', null) !== null) {
            App::setLocale($request->header('Language'));
        }
        if(Str::startsWith($request->path(),'en')){
            App::setLocale('en');
        }


        return $next($request);
    }
}

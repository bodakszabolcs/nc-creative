<?php

namespace App\Contracts;

/**
 * Base model interface for System models.
 * Interface BaseModelInterface
 * @package App\Contracts
 */
interface BaseModelInterface
{
    /**
     * Eloquent model search interface.
     * @param array $filter
     * @return mixed
     */
    public function searchInModel(array $filter);

    /**
     * Eloquent model fill data and save interface;
     * @param array $request
     * @return mixed
     */
    public function fillAndSave(array $request);

    /**
     * Eloquent model filter getter interface;
     * @return mixed
     */
    public function getFilters();
}

<?php

namespace App\Contracts;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

/**
 * Base controller interface for System Controllers.
 * Interface BaseControllerInterface
 * @package App\Contracts
 */
interface BaseControllerInterface
{
    /**
     * Base index interface.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request, $auth = null);

    /**
     * Base create interface.
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request);

    /**
     * Base show interface;
     * @param Request $request
     * @param $id
     * @param $auth
     * @return mixed
     */
    public function show(Request $request, $id, $auth);

    /**
     * Base update interface;
     * @param Request $request
     * @param $id
     * @param $auth
     * @return mixed
     */
    public function update(Request $request, $id, $auth);

    /**
     * Base destroy interface;
     * @param $id
     * @param $auth
     * @return mixed
     */
    public function destroy($id, $auth);
}

<?php

namespace App;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Support\Facades\Schema;

class MailLog extends BaseModel
{
    use Cachable;

    protected $table = 'maillogs';

    public function __construct(array $attributes = [])
    {
        if (Schema::hasTable('maillogs_'.date("Y").'_'.date("m"))) {
            $this->table = 'maillogs_'.date("Y").'_'.date("m");
        }
        parent::__construct($attributes);
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'to',
        'subject',
        'message',
        'files',
        'order_id'

    ];

    public function user()
    {
        return $this->belongsTo('Modules\User\Entities\User','to','email');
    }
}

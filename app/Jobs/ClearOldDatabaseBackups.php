<?php

namespace App\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ClearOldDatabaseBackups implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach(\Storage::disk('local')->allFiles('backups') as $file) {
            $modified = \Storage::lastModified($file);
            if (Carbon::createFromTimestamp($modified)->toDateTimeString() < date("Y-m-d H:i:s", strtotime("-10 days"))) {
                \Storage::delete($file);
            }
        }
    }
}

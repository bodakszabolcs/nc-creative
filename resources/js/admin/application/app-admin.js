/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import '@babel/polyfill'
import Es6Promise from 'es6-promise'
import 'es6-promise/auto'
import VueAxios from 'vue-axios'
import axios from 'axios'
import router from '../router'
import VueI18n from 'vue-i18n'
import store from '../../store'
import App from '../components/App.vue'
import VueCookies from 'vue-cookies'
import i18n from '../../translate'
import FileManager from 'laravel-file-manager'

// Global 3rd party plugins
import 'bootstrap'
import 'popper.js'
import 'tooltip.js'
import 'perfect-scrollbar'
import 'select2'
import 'moment'
import 'bootstrap-datepicker'
import 'bootstrap-daterangepicker'
import 'bootstrap-datetime-picker'

Es6Promise.polyfill()

require('./common/plugins/bootstrap-vue')
require('./common/plugins/perfect-scrollbar')
require('./bootstrap-admin')
require('./axios-interceptor')
require('../../admin/modules')
require('../../translation')

window.Vue = require('vue')

Vue.use(VueAxios, axios, VueI18n, router, store, VueCookies)

Vue.use(FileManager, { store })

const mixin = require('../../mixins')
Vue.mixin(mixin.default)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.config.productionTip = false
Vue.prototype.$translate = i18n
Vue.prototype.$eventHub = new Vue()

new Vue({
    el: '#app',
    router,
    VueAxios,
    axios,
    VueCookies,
    store,
    i18n,
    render: (h) => h(App)
})

import Admin from '../../../Modules/Admin/Resources/assets/js/components/Admin'
import NotFound from '../../../Modules/Admin/Resources/assets/js/components/NotFound'

const mixedroutes = []

/* ROUTE CONFIG START */
const routesAdmin = require('./../../../Modules/Admin/Resources/assets/js/router/index')
const routesSlider = require('./../../../Modules/Slider/Resources/assets/js/router/index')
const routesTranslation = require('./../../../Modules/Translation/Resources/assets/js/router/index')
const routesSystem = require('./../../../Modules/System/Resources/assets/js/router/index')
const routesUsers = require('./../../../Modules/User/Resources/assets/js/router/index')
const routesRoles = require('./../../../Modules/Role/Resources/assets/js/router/index')
const routesModuleBuilder = require('./../../../Modules/ModuleBuilder/Resources/assets/js/router/index')
const routesPage = require('./../../../Modules/Page/Resources/assets/js/router/index')
const routesMenu = require('./../../../Modules/Menu/Resources/assets/js/router/index')
const routesBlog = require('./../../../Modules/Blog/Resources/assets/js/router/index')
const routesCategory = require('./../../../Modules/Category/Resources/assets/js/router/index')
const routesMessage = require('./../../../Modules/Message/Resources/assets/js/router/index')
const routesCookie = require('./../../../Modules/Cookie/Resources/assets/js/router/index')

const routesGalery = require('../../../Modules/Galery/Resources/assets/js/router/index')

const routesGaleryImages = require('./../../../Modules/GaleryImages/Resources/assets/js/router/index')
const routesMember = require('./../../../Modules/Member/Resources/assets/js/router/index')
const routesPartners = require('./../../../Modules/Partners/Resources/assets/js/router/index')
const routesReview = require('./../../../Modules/Review/Resources/assets/js/router/index')
const routesTopblock = require('./../../../Modules/Topblock/Resources/assets/js/router/index')
/* ROUTE CONFIG END */

/* ROUTE ARRAY START */
const routes = [
    routesSystem.default, routesTranslation.default, routesAdmin.default, routesUsers.default, routesRoles.default, routesModuleBuilder.default,
    routesPage.default, routesMenu.default, routesBlog.default, routesCategory.default, routesMessage.default, routesCookie.default, routesSlider.default, routesGalery.default, routesGaleryImages.default, routesMember.default, routesPartners.default, routesReview.default, routesTopblock.default] // routesEnd
/* ROUTE ARRAY END */
for (const it in routes) {
    for (const elem in routes[it]) {
        let have = false
        for (const find in mixedroutes) {
            if (mixedroutes[find].path === routes[it][elem].path) {
                have = true
                mixedroutes[find].children = mixedroutes[find].children.concat(routes[it][elem].children)
            }
        }
        if (!have) {
            mixedroutes.push(routes[it][elem])
        }
    }
}
mixedroutes.push(
    {
        path: '/admin',
        component: Admin,
        meta: {
            title: 'Admin',
            requiresAuth: true
        },
        children: [{
            path: '*',
            component: NotFound,
            meta: {
                title: 'A keresett oldal nem található'
            }
        }
        ]
    })
export default mixedroutes

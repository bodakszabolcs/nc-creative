import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import store from '../../store'

Vue.use(VueRouter)

const routes = require('./../routes')

const router = new VueRouter({
    mode: 'history',
    routes: routes.default
})

axios.get(`${process.env.MIX_APP_URL}/storage/prefixes.json`).then(response => {
    store.commit('defaultStore/setPrefixes', response.data)
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if ($cookies.get('token') === null) {
            next({
                path: `/${process.env.MIX_ADMIN_URL}/login`,
                query: { redirect: to.fullPath }
            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
})

export default router

import store from '../store'
import VueI18n from 'vue-i18n'
window.Vue = require('vue')

const messages = store.state.defaultStore.languages

Vue.use(VueI18n)

let language = process.env.MIX_APP_DEFAULT_LOCALE
if (localStorage.getItem('language')) {
    language = localStorage.getItem('language')
}

export default new VueI18n({
    locale: language, // set locale
    fallbackLocale: process.env.MIX_APP_DEFAULT_LOCALE,
    silentTranslationWarn: true,
    messages: messages
})

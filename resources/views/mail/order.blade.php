@component('mail::message')
    <h1> {{__('Kedves ').$order->details->shipping_name }},</h1>
    <p>{{__('A megrendelést rögzítettük, a rendelés aktuális állapotáról folyamatosan tájékoztatjuk e-mail-ben.')}}</p>
    <table  class="table" style="width:100%;text-align: left;border: solid 1px #f29f1c;">
        <tr>
            <th style="padding:5px;background: #f29f1c;color: white;font-weight: 700;font-size: x-large;">{{__('Shipping address')}}</th>
            <th style="padding:5px;background: #f29f1c;color: white;font-weight: 700;font-size: x-large;">{{__('Billing address')}}</th>
        </tr>
        <tr>
            <td style="padding:5px;border-bottom: solid 1px #f29f1c;">
                <b>{{$order->details->shipping_name}}</b><br/>
                {{$order->details->shipping_email}}<br/>
                {{$order->details->shipping_phone}}<br/>
                {{$order->details->shipping_zip}} {{$order->details->shipping_city}} {{$order->details->shipping_address}}<br />
                {{$order->details->shipping_country}}<br/>
               @if($order->details->shipping_note){{__('Comment')}}: {{$order->details->shipping_note}} @endif
</td>
            <td style="padding:5px; border-bottom: solid 1px #f29f1c;">
                <b>{{$order->details->billing_name}}</b><br/>
                {{$order->details->billing_email}}<br/>
                {{$order->details->billing_phone}}<br/>
                {{$order->details->billing_zip}} {{$order->details->billing_city}} {{$order->details->billing_address}}<br />
                {{$order->details->billing_country}}<br/>
                {{$order->details->billing_note}}
</td>
        </tr>
            <tr>
            <td colspan="2" style="padding:5px; ">
                <b>{{__('Shipping method')}}</b>: {{optional($order->shippingMethod)->name}}</td>
            </tr>
        <tr>
            <td colspan="2" style="padding:5px; ">
                <b> {{__('Payment method')}}</b>: {{optional($order->paymentMethod)->name}}</td>
        </tr>
    </table>
    <table class = "table" style="width:100%;text-align: left;margin-top:15px;margin-bottom:50px">
        <thead>
        <tr>
            <th class = "product-name" colspan="2" style="text-align:left">{{__('Product')}}</th>
            <th class = "product-price">{{__('Price')}}</th>
            <th class = "product-quantity">{{__('Qty')}}</th>
            <th class = "product-subtotal">{{__('Total')}}</th>
        </tr>
        </thead>
        <tbody>@foreach($order->items as $item)<tr>
            <td style="border-bottom: solid 1px #edeff2" class = "product-thumbnail">
                <img style="height: 50px" src = "{{url($item->variation->main_image)}}">
            </td>
            <td style="border-bottom: solid 1px #edeff2" class = "product-name" data-title = "Product">{{$item->variation->product->name}}</td>
            <td style="border-bottom: solid 1px #edeff2;white-space: nowrap" class = "product-price" data-title = "Price">{{price_format($item->unit_price,1)}}</td>
            <td style="border-bottom: solid 1px #edeff2" class = "product-quantity" data-title = "Quantity">{{$item->quantity}}</div></td>
            <td style="border-bottom: solid 1px #edeff2;white-space: nowrap" class = "product-subtotal" data-title = "Total">{{price_format($item->unit_price*$item->quantity,1)}}</td>
        </tr>@endforeach
        <tr>
            <td colspan="3"></td>
            <td style="text-align: right;white-space: nowrap">{{__('Subtotal')}}: </td>
            <td style="white-space: nowrap">{{ price_format($order->order_total- $order->discount- $order->shipping_price,1)}}</td>
        </tr>
        @if($order->discount)<tr>
            <td colspan="3"></td>
            <td style="text-align: right;white-space: nowrap">{{__('Discount')}}:</td>
            <td style="white-space: nowrap"> {{ price_format(- $order->discount,1)}}</td>
        </tr>
        @endif
        <tr>
            <td colspan="3"></td>
            <td style="text-align: right;white-space: nowrap">{{__('Shipping price')}}:</td>
            <td style="white-space: nowrap"> {{ price_format($order->shipping_price,1)}}</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td style="text-align: right;white-space: nowrap">{{__('Order total')}}:</td>
            <td style="white-space: nowrap"> <b>{{ price_format($order->order_total,1)}}</b></td>
        </tr>
        </tbody>
    </table>

{{__('Üdvözlettel')}},<br>
{{__('Magyar Numzmatikai társulat ')}}
@endcomponent

@component('mail::message')
    <h1> {{__('NK creative kapcsolatfelvétel ').$message->name }},</h1>


 Név: {!! $message->name !!}<br>
 Email: {!! $message->email !!}<br>
 Tárgy: {!! $message->subject !!}<br>
 üzenet: {!! $message->message !!}<br>
 Fotózás dátuma: {!! $message->date !!}<br>
 Fotózás helyszin: {!! $message->location  !!}<br>
 Hogyan talált ránk: {!! $message->info  !!}<br>

{{__('Üdvözlettel')}},<br>
{{__('NK Creative ')}}
@endcomponent

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script type="text/javascript" src="/frontend/js/instafeed.min.js"></script>
    {!! (isset($meta)) ? $meta : '' !!}

    <!-- Scripts -->
   <?php /* <!--<script src="{{ mix('js/app.js') }}" defer></script>--> */?>

    <!-- Styles -->
    <?php
    $dir =  public_path('static');
    $files = scandir($dir.'/css');

    ?>
    @foreach($files as $file )
        @if(!\Illuminate\Support\Str::contains($file,'.map') && $file !=='.' && $file !='..')
        <link href="/static/css/{{$file}}"  rel=stylesheet>
            <link rel="preload" href="https://nkcreative.hu/static/css/{{$file}}" as="style" onload="this.onload=null;this.rel='stylesheet'">
            <noscript><link rel="stylesheet" href="http://nkcreative.hu/static/css/{{$file}}"></noscript>
        @endif
    @endforeach
   <?php /* <!--<link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('css/frontend.css') }}" rel="stylesheet">
    <link href="{{ mix('css/vendor.css') }}" rel="stylesheet">
    -->
    */?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-PELZ5J5X84"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-PELZ5J5X84');
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NHVBHHT');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NHVBHHT"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div id="app" class="application-container">
        <!-- begin:: Page -->
        <!-- end:: Page -->
    </div>
   <?php /* <!--<script src="{{mix('js/mixed.js')}}" defer></script>--> */?>
   <?php
   $dir =  public_path('static');
   $files = scandir($dir.'/js');

   ?>
    @foreach(array_reverse($files) as $file )
        @if(\Illuminate\Support\Str::contains($file,'manifest') && !\Illuminate\Support\Str::contains($file,'.map') && $file !=='.' && $file !='..')
            <script type=text/javascript src="https://nkcreative.hu/static/js/{{$file}}" defer></script>
        @endif
    @endforeach
    @foreach(array_reverse($files) as $file )
        @if(\Illuminate\Support\Str::contains($file,'vendor') && !\Illuminate\Support\Str::contains($file,'.map') && $file !=='.' && $file !='..')
            <script type=text/javascript src="https://nkcreative.hu/static/js/{{$file}}" defer></script>
        @endif
    @endforeach
    @foreach(array_reverse($files) as $file )
        @if(\Illuminate\Support\Str::contains($file,'app') && !\Illuminate\Support\Str::contains($file,'.map') && $file !=='.' && $file !='..')
            <script type=text/javascript src="https://nkcreative.hu/static/js/{{$file}}" defer></script>
        @endif
    @endforeach


</body>
</html>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!-- begin::Head -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="{{ mix('js/app-admin.js') }}" defer></script>

    <!--begin::Fonts -->
    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700|Material+Icons" rel="stylesheet">
    <!--end::Fonts -->

    <!--begin::Layout Skins -->

    <link href="{{ mix('css/app-admin.css') }}" rel="stylesheet">

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="/assets/media/logos/favicon.ico" />
</head>
<!-- end::Head -->

<!-- begin::Body -->
<body>
<div id="app">
    <!-- begin:: Page -->
    <!-- end:: Page -->
</div>
</body>
<!-- end::Body -->
</html>

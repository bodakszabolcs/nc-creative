module.exports = {
    env: {
        browser: true,
        es6: true,
        node: true
    },
    extends: [
        'plugin:vue/essential',
        'standard'
    ],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
        Vue: false,
        $cookies: false,
        KTApp: false,
        toastr: false,
        $: false,
        CKEDITOR: false,
        swal: false,
        moment: false,
        Morris: false,
        ace: false
    },
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module'
    },
    plugins: [
        'vue'
    ],
    rules: {
        "indent": ["error", 4],
        "max-len": ["off", {"code": 100}],
        "no-new": "off",
        "vue/no-unused-components": "off",
        "no-prototype-builtins": "off"
    }
}

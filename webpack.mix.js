const mix = require('laravel-mix')
/*
 |--------------------------------------------------------------------------
 | Admin Asset Management
 |--------------------------------------------------------------------------
 */

mix.js(['resources/js/admin/application/app-admin.js'], 'public/js')
    .sass('resources/sass/app-admin.scss', 'public/css')
    .options({ processCssUrls: false })

if (mix.inProduction()) {
    mix.version()

    mix.webpackConfig({
        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.jsx?$/,
                    exclude: /node_modules/,
                    include: [
                        path.resolve('node_modules/laravel-file-manager'),
                        path.resolve('node_modules/engine.io-client')
                    ],
                    use: [{
                        loader: 'babel-loader',
                        options: Config.babel()
                    }]
                }
            ]
        }
    })
} else {
    mix.webpackConfig({
        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /node_modules/
                }
            ]
        }
    })
}

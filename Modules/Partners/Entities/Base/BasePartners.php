<?php

namespace Modules\Partners\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BasePartners extends BaseModel
{
    

    protected $table = 'partners';

    protected $dates = ['created_at', 'updated_at'];

    

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'name',
                    'title' => 'Name',
                    'type' => 'text',
                    ],[
                    'name' => 'description',
                    'title' => 'Description',
                    'type' => 'text',
                    ],[
                    'name' => 'image',
                    'title' => 'Image',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['name','description','image'];

    protected $casts = [];

    
}

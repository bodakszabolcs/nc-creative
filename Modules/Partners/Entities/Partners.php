<?php

namespace Modules\Partners\Entities;

use Modules\Partners\Entities\Base\BasePartners;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class Partners extends BasePartners
{
    use SoftDeletes, Cachable, HasTranslations;





    public $translatable = ["name","description"];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }








}

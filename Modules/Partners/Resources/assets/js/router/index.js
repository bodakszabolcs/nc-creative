import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Partners from '../components/Partners'
import PartnersList from '../components/PartnersList'
import PartnersCreate from '../components/PartnersCreate'
import PartnersEdit from '../components/PartnersEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'partner',
                component: Partners,
                meta: {
                    title: 'Partners'
                },
                children: [
                    {
                        path: 'index',
                        name: 'PartnersList',
                        component: PartnersList,
                        meta: {
                            title: 'Partners',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/partner/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/partner/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'PartnersCreate',
                        component: PartnersCreate,
                        meta: {
                            title: 'Create Partners',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/partner/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'PartnersEdit',
                        component: PartnersEdit,
                        meta: {
                            title: 'Edit Partners',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/partner/index'
                        }
                    }
                ]
            }
        ]
    }
]

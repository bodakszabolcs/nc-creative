<?php

namespace Modules\Partners\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Partners\Entities\Partners;
use Illuminate\Http\Request;
use Modules\Partners\Http\Requests\PartnersCreateRequest;
use Modules\Partners\Http\Requests\PartnersUpdateRequest;
use Modules\Partners\Transformers\PartnersViewResource;
use Modules\Partners\Transformers\PartnersListResource;

class PartnersController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Partners();
        $this->viewResource = PartnersViewResource::class;
        $this->listResource = PartnersListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = PartnersCreateRequest::class;
        $this->updateRequest = PartnersUpdateRequest::class;
    }
    public function getLogos(Request $request){
        return PartnersListResource::collection(Partners::orderBy('updated_at','desc')->get());
    }

}

<?php

namespace Modules\Partners\Observers;

use Modules\Partners\Entities\Partners;

class PartnersObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Partners\Entities\Partners  $model
     * @return void
     */
    public function saved(Partners $model)
    {
        Partners::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Partners\Entities\Partners  $model
     * @return void
     */
    public function created(Partners $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Partners\Entities\Partners  $model
     * @return void
     */
    public function updated(Partners $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Partners\Entities\Partners  $model
     * @return void
     */
    public function deleted(Partners $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Partners\Entities\Partners  $model
     * @return void
     */
    public function restored(Partners $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Partners\Entities\Partners  $model
     * @return void
     */
    public function forceDeleted(Partners $model)
    {
        //
    }
}

<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Partners\Entities\Partners;

$factory->define(Partners::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),
"description" => $faker->realText(),
"image" => $faker->realText(),

    ];
});

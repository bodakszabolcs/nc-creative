<?php

return [
    'name' => 'Partners',

                 'menu_order' => 20,

                 'menu' => [
                     [

                      'icon' =>'flaticon-squares',

                      'title' =>'Partners',

                      'route' =>'/'.env('ADMIN_URL').'/partner/index',

                     ]

                 ]
];

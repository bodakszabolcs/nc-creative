<?php

namespace Modules\Partners\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class PartnersListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "description" => $this->description,
		    "image" => str_replace('https://','https://cdn'.rand(1,4).'.',env('APP_URL')).$this->image,
		     ];
    }
}

<?php

namespace Modules\Partners\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class PartnersViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->getTranslatable('name'),
		    "description" => $this->getTranslatable('description'),
		    "image" => $this->image,
		"selectables" => [
		]
		     ];
    }
}

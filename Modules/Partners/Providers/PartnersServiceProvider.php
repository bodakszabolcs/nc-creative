<?php

namespace Modules\Partners\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Partners\Entities\Partners;
use Modules\Partners\Observers\PartnersObserver;

class PartnersServiceProvider extends ModuleServiceProvider
{
    protected $module = 'partners';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Partners::observe(PartnersObserver::class);
    }
}

import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Cookie from '../components/Cookie'
import CookieCreate from '../components/CookieCreate'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'cookie',
                component: Cookie,
                meta: {
                    title: 'Cookies'
                },
                children: [
                    {
                        path: 'index/:lang?',
                        name: 'CookieCreate',
                        component: CookieCreate,
                        meta: {
                            title: 'Create Cookies',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/cookie/index'
                        }
                    }
                ]
            }
        ]
    }
]

@if(isset($_COOKIE['cookies']))

    @php
    try {
         $language = \Illuminate\Support\Facades\Request::header('Language','hu');
         $jsonFile = json_decode(file_get_contents(public_path('cookie/cookie-'.$language.'.json')),1);
         $cookie = json_decode(base64_decode($_COOKIE['cookies']),1);
    @endphp
    @if($cookie['cookieMarketing'])
        {!! $jsonFile['marketing_scripts'] !!}
    @endif
    @if(!empty($jsonFile['custom_css']))
        {!! $jsonFile['custom_css'] !!}
    @endif
    @php
       } catch (\Exception $e){
        Log::error($e->getMessage());
        echo '<div class="alert alert-danger text-center">Cookie plugin not loaded<div/>';
        }
    @endphp


@endif

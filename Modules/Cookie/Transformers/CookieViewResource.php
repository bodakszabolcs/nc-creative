<?php

namespace Modules\Cookie\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class CookieViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [

		    "title" => $this->title,
		    "lead" => $this->lead,
		    "link" => $this->link,
		    "custom_css" => $this->custom_css,
		    "position" => $this->position,
		    "session_title" => $this->session_title,
		    "session_description" => $this->session_description,
		    "analytics_title" => $this->analytics_title,
		    "analytics_description" => $this->analytics_description,
		    "analytics_scripts" => $this->analytics_scripts,
		    "marketing_title" => $this->marketing_title,
		    "marketing_description" => $this->marketing_description,
		    "marketing_scripts" => $this->marketing_scripts,
            "selectables" =>[
                'positions' => [
                    'top' => __('Top'),
                    'bottom' => __('Bottom')
                ]
            ]
		     ];
    }
}

<?php

return [
    'name' => 'Cookie',

                 'menu_order' => 22,

                 'menu' => [
                     [

                      'icon' =>'fas fa-cookie',

                      'title' =>'Cookie',

                      'route' =>'/'.env('ADMIN_URL').'/cookie/index',

                     ]

                 ]
];

<?php

namespace Modules\Cookie\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CookieCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'lead' => 'required',
            'link' => 'required',
            'session_title' => 'required',
            'session_description' => 'required',

        ];
    }

    public function attributes()
    {
        return [
            'title' => __('Title'),
            'lead' => __('lead'),
            'link' => __('Link'),
            'custom_css' => __('Custom style'),
            'settings' => __('Settings'),
            'session_title' => __('Session cookie title'),
            'session_description' => __('Session cookie description'),
            'analytics_title' => __('Analitycs cookie title'),
            'analytics_description' => __('Analitycs cookie description'),
            'analytics_scripts' => __('Analytics scripts'),
            'marketing_title' => __('Marketing cookie title'),
            'marketing_descrription' => __('Marketing description'),
            'marketing_scripts' => __('Marketing scripts'),

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

<?php

namespace Modules\Cookie\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use App\Http\Controllers\Controller;
use Modules\Cookie\Entities\Cookie;
use Illuminate\Http\Request;
use Modules\Cookie\Http\Requests\CookieCreateRequest;
use Modules\Cookie\Http\Requests\CookieUpdateRequest;
use Modules\Cookie\Transformers\CookieViewResource;
use Modules\Cookie\Transformers\CookieListResource;

class CookieController extends Controller
{

    public function index(Request $request, $lang = 'hu')
    {
        $cookie = new Cookie($lang);
        return new CookieViewResource($cookie);
    }

    public function saveCookie(CookieCreateRequest $request, $lang = 'hu')
    {
        $cookie = new Cookie($lang);
        $cookie->fillAndSave($request->all(), $lang);
        return response()->json(['message' => __('Cookie saved')]);
    }

    public function publishCookie(Request $request, $lang = 'hu')
    {
        $cookie = new Cookie($lang);
        $cookie->fillAndSave($request->all(), $lang);
        $cookie->publish($lang);
        return response()->json(['message' => __('Cookie published')]);
    }
}

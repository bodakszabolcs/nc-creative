<?php


Route::group(['namespace' => 'Modules\Cookie\Http\Controllers', 'prefix' => 'cookie', 'middleware' => ['auth:sanctum']], function () {

    Route::post('/save/{lang?}', 'CookieController@saveCookie')->name('Save cookie');
    Route::get('/index/{lang?}', 'CookieController@index')->name('List cookie');

    Route::post('/publish/{lang?}', 'CookieController@publishCookie')->name('Publish cookie');

});

<?php

namespace Modules\Cookie\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Cookie\Entities\Cookie;
use Modules\Cookie\Observers\CookieObserver;

class CookieServiceProvider extends ModuleServiceProvider
{
    protected $module = 'cookie';
    protected $directory = __DIR__;
    protected $isRegisterViews = true;

    public function boot()
    {
        parent::boot();
    }
}

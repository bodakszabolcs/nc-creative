<?php

namespace Modules\Cookie\Entities;


use Illuminate\Support\Facades\Storage;

class Cookie
{
    public $title = '';
    public $lead = '';
    public $link = '';
    public $custom_css = '';
    public $position = '';
    public $session_title = '';
    public $session_description = '';
    public $analytics_title = '';
    public $analytics_description = '';
    public $analytics_scripts = '';
    public $marketing_title = '';
    public $marketing_description = '';
    public $marketing_scripts = '';

    private $fileName = 'cookie';
    public function __construct($lang='hu')
    {

        $exists = Storage::disk('cookie')->exists($this->fileName.'-'.$lang.'.json');
        if($exists){
            $content = Storage::disk('cookie')->get($this->fileName.'-'.$lang.'.json');
            $content = \json_decode($content,1);
             //foreach ($lang in $langs)
            foreach ($content as $k => $v){
                if(isset($this->{$k})){
                    $this->{$k} = $v;
                }
            }
        }
    }
    public function publish($lang='hu'){
        $exists = Storage::disk('cookie')->exists($this->fileName.'-'.$lang.'.json');
        if($exists) {
            $file = storage_path('cookie/'.$this->fileName.'-'.$lang.'.json');
            $destination = public_path('cookie/'.$this->fileName.'-'.$lang.'.json');
            if (!file_exists(public_path('cookie'))) {
                mkdir(public_path('cookie'), 0777, true);
            }
            copy($file,$destination);
        }
    }
    public function fillAndSave(array $array=[],$lang ){
        foreach ($array as $k => $v){
            if(isset($this->{$k})){
                $this->{$k} = $v;
            }
        }
        $reflect = new \ReflectionClass($this);
        $props = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC);
        $saveObject= [];

        foreach ($props as $p){
            $saveObject[$p->name] = $this->{$p->name};
        }
        Storage::disk('cookie')->put($this->fileName.'-'.$lang.'.json', json_encode($saveObject));
    }

}

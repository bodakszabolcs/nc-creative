<?php

namespace Modules\Page\Transformers;

use App\Http\Resources\BaseResource;

class PageViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->getTranslatable('name'),
            'content' => $this->getTranslatable('content'),
            'slug' => $this->slug,
            'meta_title' => $this->getTranslatable('meta_title'),
            'meta_description' => $this->getTranslatable('meta_description'),
            'og_image' => $this->og_image,
            'component_name' => $this->getTranslatable('component_name'),
        ];
    }
}

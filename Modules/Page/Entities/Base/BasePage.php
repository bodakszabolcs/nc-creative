<?php

namespace Modules\Page\Entities\Base;

use App\BaseModel;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Blog\Entities\Blog;
use Modules\Category\Entities\Category;
use Modules\Galery\Entities\Galery;
use Modules\Page\Entities\Page;
use Modules\System\Entities\Settings;
use PHPHtmlParser\Dom;

abstract class BasePage extends BaseModel
{
    protected $table = 'pages';

    protected $fillable = [
        'name',
        'slug',
        'content',
        'meta_title',
        'meta_description',
        'og_image',
        'component_name'
    ];

    public function fillAndSave(array $request)
    {
        $this->fill($request);
        $this->save();

        if (Arr::get($request, 'slug', null) == null) {
            $this->slug = $this->slugify($this->name.' '.$this->id);
        }

        $this->save();

        return $this;
    }

    public function sanitizeContent($key, $content) {
        $dom = new Dom();
        $dom->load($content);

        return $dom->find('*[data-name='.$key.']')[0]->innerHtml;
    }

    public function getTemplateSchemes() {
        if (is_null($this->component_name) || (is_string($this->content) && json_decode($this->content) === false)) {
            return false;
        }

        $baseScheme = [];

        $content = Storage::disk('resources')->get('js/frontend/components/pages/' . $this->component_name);

        $vueContent = $this->get_string_between($content, '<template>','</template>');

        $vueContent = trim(str_replace("\n",'',$vueContent));

        $dom = new Dom();
        $dom->load($vueContent);

        $elements = $dom->find('*[data-name]');

        foreach ( $elements as $elem) {
            $keyName = $elem->getAttribute('data-name');
            $html = $elem->innerHtml;

            $baseScheme[$keyName] = $html;
        }

        $this->content = json_encode($baseScheme);
        $this->saveWithoutEvents();
    }

    public function saveEditorData($request) {
        $content = $this->content;

        if (json_decode($content) !== false) {
            $content = json_decode($content, true);
        } else {
            $content = [];
        }

        foreach ($request->all() as $key => $val) {
            $content[$key] = $this->sanitizeContent($key, $val);
        }

        $this->content = json_encode($content);
        $this->save();
    }

    public static function returnFrontendContent($request)
    {
        $meta = '<title>' . config('app.name', 'Laravel') . '</title>';
        $path = $request->path();

        if($path != '/' && $path != 'en' && $path != 'galeria' && $path != 'en/galeria' && !Str::contains($path,'galeria') &&  !Str::contains($path,'portfolio')){
            abort(404);
        }
        $replacedPath = Str::replaceFirst("/", "", $path);
        if(app::getLocale()=='hu') {
            $meta .= '<meta name="description" content="NK-Creative Photography & Cinematography | Profi fotózás: esküvői fotózás, termék fotózás, kreatív fotózás, gyerek fotózás, kismama fotózá, portré">';
        }else{
            $meta .= '<meta name="description" content="NK-Creative Photography & Cinematography | Professional photography, Wedding photography, Creative photography, Product photography, Children photography, Maternity photography, Portrait, Headshot.">';
        }
        $meta .= '<meta name="og:title" content="' . config('app.name', 'Laravel') . '">';
        $meta .= '<meta name="og:url" content="' . $request->path() . '">';
        $meta .= '<meta name="og:image" content="'.env('APP_URL').'/frontend/image/logo_light.svg">';
        /* Return Static Page */

        $slug =explode('/',$path);
        $page = Galery::where(function ($query) use ($slug, $replacedPath) {
            $query->where('slug', '=', end($slug))
                ->orWhere('slug', '=', '/'.end($slug));
        })->first();

        if (!is_null($page)) {

            $meta = '<title>' . config('app.name', 'Laravel') .$page->title.'</title>';
            $meta .= '<meta name="description" content="' . $page->meta_description . '">';
            $meta .= '<meta name="og:title" content="' . $page->title . '">';
            $meta .= '<meta name="og:url" content="' . $request->path() . '">';
            $meta .= '<meta name="og:image" content="'.str_replace('storage/','storage/thumbnails-desktop/',$page->lead_image).'">';

        }
        if(!Str::startsWith($request->path(),'en') || !Str::startsWith($request->path(),'/en')) {
            $meta.= '<link rel="alternate" hreflang="en" href="'.env('APP_URL').'/en/'.$request->path().'">';
        }else{
            $meta.= '<link rel="alternate" hreflang="hu" href="'.env('APP_URL').'/'.str_replace('en','',$request->path()).'">';
        }
        /* End of Return Static Page */

        /*$prefixes = json_decode(file_get_contents(base_path('public/storage/prefixes.json')), true);


        $blog = Blog::where('slug', 'LIKE', Str::replaceFirst(Arr::get($prefixes, 'blog',null) . "/", "", $path))->first();

        if (!is_null($blog)) {
            self::setMetaTags($meta, $request->getUri(), $blog);
            return view('layouts.app', ['meta' => $meta]);
        }

        $category = Category::where('slug', 'LIKE',
            Str::replaceFirst(Arr::get($prefixes, 'category',null) . "/", "", $path))->first();

        if (!is_null($category)) {
            self::setMetaTags($meta, $request->getUri(), $category);
            return view('layouts.app', ['meta' => $meta]);
        }
        /* End of Return Category Page */

        return response()->view('layouts.app', ['meta' => $meta]);
    }

    /**
     * Generate SEO tags
     * @param $meta
     * @param $url
     * @param $data
     */
    private static function setMetaTags(&$meta, $url, $data)
    {
        $appName = $data->meta_title . ' - ' . config('app.name', 'Laravel');
        $meta = '<title>' . $appName . '</title>';
        $meta .= '<meta name="twitter:card" content="summary" />';
        $meta .= '<meta name="twitter:site" content="@creativeagent" />';
        $meta .= '<meta name="description" content="' . $data->meta_description . '">';
        $meta .= '<meta name="og:title" content="' . $appName . '">';
        $meta .= '<meta name="og:url" content="' . $url . '">';
        if (isset($data->og_image)) {
            $meta .= '<meta name="og:image" content="' . ((!Str::startsWith($data->og_image,
                    'http')) ? url($data->og_image) : $data->og_image) . '">';
        } else {
            $meta .= '<meta name="og:image" content="' . url('/frontend/img/logo.svg') . '">';
        }
        if (isset($data->tags) && is_array($data->tags)) {
            $meta .= '<meta name="keywords" content="' . implode(",", $data->tags) . '">';
        }
    }

    function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

}

<?php

return [
    'name' => 'Page',
    'menu_order' => 1,
    'menu' => [
        [
            'icon' => 'la la-dashboard',
            'title' => 'Page',
            'route' => '/'.env('ADMIN_URL').'/page/index'
        ]
    ]
];

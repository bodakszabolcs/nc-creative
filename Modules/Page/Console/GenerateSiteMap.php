<?php

namespace Modules\Page\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Modules\Blog\Entities\Blog;
use Modules\Category\Entities\Category;
use Modules\Galery\Entities\Galery;
use Modules\Page\Entities\Page;
use Modules\System\Entities\Settings;
use Modules\Webshop\Entities\Product;

class GenerateSiteMap extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'generate:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Sitemap for every url.';

    private $xml;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->xml = '<?xml version="1.0" encoding="UTF-8"?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $lang = Settings::where('settings_key','=','language')->first();
        $languages = json_decode($lang->settings_value, true);

        foreach ($languages as $key => $val) {
            App::setLocale($key);

            foreach (Galery::all() as $model) {

                $this->xml .= '<url>
                      <loc>'.url((($key=='en')?'en/galeria/':'galeria/').$model->slug).'</loc>
                      <lastmod>'.date("Y-m-d", strtotime($model->updated_at)).'T'.date("H:i:s", strtotime($model->updated_at)).'+00:00</lastmod>
                    </url>';
            }


        }

        $this->xml .= '<url><loc>'.url('galeria').'</loc><lastmod>'.date("Y-m-d").'T'.date("H:i:s").'+00:00</lastmod></url>';
        $this->xml .= '<url><loc>'.url('/').'</loc><lastmod>'.date("Y-m-d").'T'.date("H:i:s").'+00:00</lastmod></url>';
        $this->xml .= '<url><loc>'.url('en/galeria').'</loc><lastmod>'.date("Y-m-d").'T'.date("H:i:s").'+00:00</lastmod></url>';
        $this->xml .= '<url><loc>'.url('en/').'</loc><lastmod>'.date("Y-m-d").'T'.date("H:i:s").'+00:00</lastmod></url>';
        $this->xml .= '</urlset>';

        Storage::disk('public_folder')->put('sitemap.xml', $this->xml);
        $this->info('Sitemap generated!');
    }
}

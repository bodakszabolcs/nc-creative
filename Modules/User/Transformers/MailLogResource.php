<?php

namespace Modules\User\Transformers;

use App\Http\Resources\BaseResource;

class MailLogResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'to' => $this->to,
            'subject' => $this->subject,
            'user_name' => $this->user->getName(),
            'message' => $this->message,
            'files' => $this->files,
            'order_id' => $this->order_id,
            'extra' => json_encode($this->extra),
            'created_at' => format_date($this->created_at, $this->getDateFormat())
        ];
    }
}

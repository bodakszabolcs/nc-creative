<?php

namespace Modules\User\Transformers;

use App\Http\Resources\BaseResource;

class AccessLogResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'action' => $this->action,
            'user_id' => $this->user_id,
            'user_name' => optional($this->user)->getName(),
            'user' => $this->user,
            'route' => $this->route,
            'extra' => json_encode($this->extra),
            'created_at' => format_date($this->created_at, $this->getDateFormat())
        ];
    }
}

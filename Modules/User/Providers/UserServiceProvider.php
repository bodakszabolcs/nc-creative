<?php

namespace Modules\User\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\User\Entities\User;
use Modules\User\Observers\UserObserver;

class UserServiceProvider extends ModuleServiceProvider
{
    protected $module = 'user';
    protected $directory = __DIR__;

    public function boot()
    {
        $this->isRegisterViews = true;
        parent::boot();

        User::observe(UserObserver::class);
    }

}

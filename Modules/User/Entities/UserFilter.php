<?php

namespace Modules\User\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Modules\User\Entities\Base\BaseUserFilter;

class UserFilter extends BaseUserFilter
{
    use Cachable;
}

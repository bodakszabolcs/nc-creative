<?php

namespace Modules\User\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Entities\Base\BaseUserShipping;

class UserShipping extends BaseUserShipping
{
    use Cachable, SoftDeletes;
}

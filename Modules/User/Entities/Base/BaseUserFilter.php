<?php

namespace Modules\User\Entities\Base;

use App\BaseModel;

abstract class BaseUserFilter extends BaseModel
{
    protected $table = 'user_filters';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'path',
        'params',
        'query'

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'params' => 'array',
        'query' => 'array'
    ];
}

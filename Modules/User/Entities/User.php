<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Entities\Base\BaseUser;
use Modules\Webshop\Entities\Cart;
use Modules\Webshop\Entities\Country;
use Modules\Webshop\Entities\Order;
use Modules\Webshop\Entities\Variation;
use Modules\Webshop\Entities\VariationPrice;
class User extends BaseUser
{
    use SoftDeletes;
    public function getCartTotal($currency=1,$country=1){
        $cart = Cart::where('user_id','=',$this->id);
        $country = Country::where('id','=',$country)->first();
        $sum =0;
        foreach ($cart->items() as $cartItem){
            if(!$cartItem->connected_products){
                $price = VariationPrice::getVariationPrice($cartItem->variation_id,$currency,$country,null,$this->id);
                $sum = $price['price_sale'];
            }else{
                $price =  Variation::where('id','=',$cartItem->variation_id)->prices()->where('currency_id','=',$currency)->first();
                $sum += $price->price_discount_net*("1.".$country->tax_rate);

            }
        }
        return $sum;

    }
    public function getOrderTotal($currency){
        return Order::where('currency_id','=',$currency)->where('user_id','=',$this->id)->where('order_status_id','=',18)->sum('order_total');

    }

}

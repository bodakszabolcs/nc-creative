<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'namespace' => 'Modules\User\Http\Controllers',
    'prefix' => '/user',
    'middleware' => ['auth:sanctum', 'accesslog', 'role']
], function () {
    Route::get('/accesslogs/{id?}', 'UserController@accessLogs')->name('User activities');
    Route::get('/maillogs/{id?}', 'UserController@mailLogs')->name('User messages');


    Route::post('/generate-password/{id}', 'UserController@generatePassword')->name('Generate Password');
    Route::post( '/force-login/{id?}', 'UserController@forceLogin')->name('Login as the selected user');
});

Route::group([
    'namespace' => 'Modules\User\Http\Controllers',
    'prefix' => '/user',
    'middleware' => ['auth:sanctum']
], function () {
    Route::get( '/', 'UserController@getUser');
    Route::post( '/filter/save', 'UserController@saveFilter');
    Route::delete( '/filter/delete/{id}', 'UserController@deleteFilter');

    Route::match(['get'], '/profile', 'UserController@getProfile')->name('Profile show');
    Route::match(['put'], '/update-profile', 'UserController@updateProfile')->name('Profile update');
    Route::match(['put'], '/change-password', 'UserController@changePassword')->name('Profile change password');
    Route::delete('/delete-profile', 'UserController@deleteProfile')->name('User delete profile');

    Route::get('/profile/accesslogs', 'UserController@accessLogsProfile')->name('Profile activities');
    Route::get('/profile/maillogs', 'UserController@mailLogsProfile')->name('Profile messages');
});

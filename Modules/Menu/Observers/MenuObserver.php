<?php

namespace Modules\Menu\Observers;

use Modules\Menu\Entities\Menu;

class MenuObserver
{
    public function saved(Menu $menu)
    {
        $menu->generateMenuJson();
    }

    /**
     * Handle the blog "deleted" event.
     *
     * @param \Modules\Menu\Entities\Menu $menu
     * @return void
     */
    public function deleted(Menu $menu)
    {
        $menu->deleteMenuJson();
    }

    /**
     * Handle the blog "restored" event.
     *
     * @param \Modules\Menu\Entities\Menu $menu
     * @return void
     */
    public function restored(Menu $menu)
    {
        $menu->generateMenuJson();
    }

    /**
     * Handle the blog "force deleted" event.
     *
     * @param \Modules\Menu\Entities\Menu $menu
     * @return void
     */
    public function forceDeleted(Menu $menu)
    {
        $menu->deleteMenuJson();
    }
}

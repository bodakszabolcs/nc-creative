import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Menu from '../components/Menu'
import MenuList from '../components/MenuList'
import MenuCreate from '../components/MenuCreate'
import MenuEdit from '../components/MenuEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'menu',
                component: Menu,
                meta: {
                    title: 'Menus'
                },
                children: [
                    {
                        path: 'index',
                        name: 'MenuList',
                        component: MenuList,
                        meta: {
                            title: 'Menus',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/menu/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/menu/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'MenuCreate',
                        component: MenuCreate,
                        meta: {
                            title: 'Create Menu',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/menu/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'MenuEdit',
                        component: MenuEdit,
                        meta: {
                            title: 'Edit Menu',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/menu/index'
                        }
                    }
                ]
            }
        ]
    }
]

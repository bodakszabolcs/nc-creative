<?php

namespace Modules\Menu\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Menu\Entities\Base\BaseMenu;

class Menu extends BaseMenu
{
    use SoftDeletes, Cachable;
}

<?php

namespace Modules\Menu\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Modules\Menu\Entities\Base\BaseItem;
use Spatie\Translatable\HasTranslations;
class Item extends BaseItem
{
    use Cachable,HasTranslations;

    public $translatable = ['title'];
}

<?php

namespace Modules\Menu\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Menu\Entities\Menu;
use Modules\Menu\Observers\MenuObserver;

class MenuServiceProvider extends ModuleServiceProvider
{
    protected $module = 'menu';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Menu::observe(MenuObserver::class);
    }
}

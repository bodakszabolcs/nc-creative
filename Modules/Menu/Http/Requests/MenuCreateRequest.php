<?php

namespace Modules\Menu\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:menus,name,'.$this->id.',id,deleted_at,NULL',
            'items' => 'array',
            'items.*.title' => 'required',
            'items.*.link' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'name' => __('Name'),
            'items' => __('Menu items'),
            'items.*.title' => __('Item title'),
            'items.*.link' => __('Item URL'),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

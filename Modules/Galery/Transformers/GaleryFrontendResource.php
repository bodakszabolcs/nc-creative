<?php

namespace Modules\Galery\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\GaleryImages\Entities\GaleryImages;
use Modules\GaleryImages\Transformers\GaleryImagesFrontendResource;
use Modules\GaleryImages\Transformers\GaleryImagesViewResource;


class GaleryFrontendResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "description" => $this->description,
		    "lead_image" => str_replace('https://','https://cdn'.rand(1,4).'.',env('APP_URL')).$this->lead_image,
            "category_id" => $this->category_id,
            "slug" => $this->slug,
            "images" => GaleryImagesFrontendResource::collection(GaleryImages::where('galery_id',$this->id)->get()),
            "selectables" => [
                'categories'=>config('categories')
            ]
		     ];
    }
}

<?php

namespace Modules\Galery\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\GaleryImages\Entities\GaleryImages;
use Modules\GaleryImages\Transformers\GaleryImagesViewResource;


class GaleryViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->getTranslatable('name'),
		    "description" => $this->getTranslatable('description'),
		    "lead_image" => $this->lead_image,
            "category_id" => $this->category_id,
            "public" => $this->public,
            "images" => GaleryImagesViewResource::collection(GaleryImages::where('galery_id',$this->id)->get()),
            "selectables" => [
                'categories'=>json_decode(json_encode(config('categories')))
            ]
		     ];
    }
}

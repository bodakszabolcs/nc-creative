<?php

namespace Modules\Galery\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class GaleryListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "slug" => $this->slug,
		    "description" => $this->description,
            "category" => array_get(config('categories'),$this->category_id),
		    "lead_image" => str_replace('https://','https://cdn'.rand(1,4).'.',env('APP_URL')).$this->lead_image,
		     ];
    }
}

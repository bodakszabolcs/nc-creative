<?php

namespace Modules\Galery\Observers;

use Modules\Galery\Entities\Galery;

class GaleryObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Galery\Entities\Galery  $model
     * @return void
     */
    public function saved(Galery $model)
    {
        Galery::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Galery\Entities\Galery  $model
     * @return void
     */
    public function created(Galery $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Galery\Entities\Galery  $model
     * @return void
     */
    public function updated(Galery $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Galery\Entities\Galery  $model
     * @return void
     */
    public function deleted(Galery $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Galery\Entities\Galery  $model
     * @return void
     */
    public function restored(Galery $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Galery\Entities\Galery  $model
     * @return void
     */
    public function forceDeleted(Galery $model)
    {
        //
    }
}

<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Galery\Entities\Galery;

$factory->define(Galery::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),
"description" => $faker->realText(),
"lead_image" => $faker->realText(),

    ];
});

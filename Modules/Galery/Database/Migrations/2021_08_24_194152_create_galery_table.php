<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGaleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('galery');
        Schema::create('galery', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->text("name")->nullable();
            $table->text("slug")->nullable();
            $table->integer("category_id")->nullable();
            $table->integer("public")->nullable();
            $table->text("description")->nullable();
            $table->text("lead_image")->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galery');
    }
}

<?php

namespace Modules\Galery\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GaleryCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
			'description' => 'required',
			'lead_image' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'name' => __('Name'),
'description' => __('Description'),
'lead_image' => __('Lead image'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

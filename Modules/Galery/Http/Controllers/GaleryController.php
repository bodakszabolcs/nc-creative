<?php

namespace Modules\Galery\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Galery\Entities\Galery;
use Illuminate\Http\Request;
use Modules\Galery\Http\Requests\GaleryCreateRequest;
use Modules\Galery\Http\Requests\GaleryUpdateRequest;
use Modules\Galery\Transformers\GaleryViewResource;
use Modules\Galery\Transformers\GaleryListResource;

class GaleryController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Galery();
        $this->viewResource = GaleryViewResource::class;
        $this->listResource = GaleryListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = GaleryCreateRequest::class;
        $this->updateRequest = GaleryUpdateRequest::class;
    }

}

<?php

namespace Modules\Galery\Entities;

use Illuminate\Support\Str;
use Modules\Galery\Entities\Base\BaseGalery;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class Galery extends BaseGalery
{
    use SoftDeletes, Cachable, HasTranslations;


    public $translatable = ["name", "description"];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }

    public function image()
    {
        return $this->hasMany('Modules\GaleryImages\Entities\GaleryImages', 'id', 'galery_id');
    }
    public function fillAndSave(array $request)
    {
        if(!isset($request['slug']) || empty($request['slug'])){
            $request['slug'] = Str::slug($request['name']['hu']);
        }
        return parent::fillAndSave($request);
    }
}

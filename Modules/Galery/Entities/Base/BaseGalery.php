<?php

namespace Modules\Galery\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BaseGalery extends BaseModel
{


    protected $table = 'galery';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'name',
                    'title' => 'Name',
                    'type' => 'text',
                    ],[
                    'name' => 'description',
                    'title' => 'Description',
                    'type' => 'text',
                    ],[
                    'name' => 'lead_image',
                    'title' => 'Lead image',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = [];
    protected $fillable = ['name','description','lead_image','category_id','slug','public'];
    protected $casts = [];

}

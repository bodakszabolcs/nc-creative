<?php

return [
    'name' => 'Galery',

                 'menu_order' => 18,

                 'menu' => [
                     [

                      'icon' =>'flaticon-squares',

                      'title' =>'Galery',

                      'route' =>'/'.env('ADMIN_URL').'/galery/index',

                     ]

                 ]
];

<?php

namespace Modules\Galery\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Galery\Entities\Galery;
use Modules\Galery\Observers\GaleryObserver;

class GaleryServiceProvider extends ModuleServiceProvider
{
    protected $module = 'galery';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();
        Galery::observe(GaleryObserver::class);
    }
}

import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Galery from '../components/Galery'
import GaleryList from '../components/GaleryList'
import GaleryCreate from '../components/GaleryCreate'
import GaleryEdit from '../components/GaleryEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'galery',
                component: Galery,
                meta: {
                    title: 'Galeries'
                },
                children: [
                    {
                        path: 'index',
                        name: 'GaleryList',
                        component: GaleryList,
                        meta: {
                            title: 'Galeries',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/galery/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/galery/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'GaleryCreate',
                        component: GaleryCreate,
                        meta: {
                            title: 'Create Galeries',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/galery/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'GaleryEdit',
                        component: GaleryEdit,
                        meta: {
                            title: 'Edit Galeries',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/galery/index'
                        }
                    }
                ]
            }
        ]
    }
]

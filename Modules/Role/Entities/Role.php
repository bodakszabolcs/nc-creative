<?php

namespace Modules\Role\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Role\Entities\Base\BaseRole;

class Role extends BaseRole
{
    use SoftDeletes, Cachable;
}

<?php

namespace Modules\Role\Entities\Base;

use App\BaseModel;
use Illuminate\Support\Arr;

abstract class BaseRole extends BaseModel
{
    protected $table = 'roles';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    protected $fillable = [
        'name',
        'description',
        'menu',
        'access'
    ];

    protected $casts = [
        'menu' => 'array',
        'access' => 'array'
    ];

    public function users()
    {
        return $this->belongsToMany('Modules\User\Entities\User','roles_users', 'role_id', 'user_id');
    }

    public function getFilters()
    {
        $this->searchColumns = [
            [
                'name' => 'name',
                'title' => __('Name'),
                'type' => 'text'
            ],
            [
                'name' => 'description',
                'title' => __('Description'),
                'type' => 'text'
            ]
        ];

        return parent::getFilters();
    }

    public function fillAndSave(array $request)
    {
        $this->fill($request);

        $access = [];
        $menu = [];
        if (is_array(Arr::get($request, 'access', []))) {
            foreach (Arr::get($request, 'access', []) as $key => $val) {
                if ($val == false) {
                    continue;
                }
                $access[$key] = true;
            }
        }

        if (is_array(Arr::get($request, 'menu', []))) {
            foreach (Arr::get($request, 'menu', []) as $key => $val) {
                if ($val == false) {
                    continue;
                }
                $menu[$key] = true;
            }
        }

        $this->access = $access;
        $this->menu = $menu;

        $this->save();

        return $this;
    }

}

<?php

namespace Modules\Role\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Role\Entities\Role;

class RoleDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $role = Role::firstOrCreate([
            'name' => 'SuperAdmin',
            'description' => 'System wide super admin.',
            'menu' => '{}',
            'access' => '{}'
        ]);

        DB::table('roles_users')->insert([
           'user_id' => 1,
           'role_id' => $role->id
        ]);


    }
}

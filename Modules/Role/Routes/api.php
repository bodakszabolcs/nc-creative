<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace' => 'Modules\Role\Http\Controllers',
    'prefix' => '/role',
], function () {
    Route::match(['get'], '/credentials', 'RoleController@credentials')->name('Role Credentials');
    Route::match(['get'], '/role-list', 'RoleController@roleList')->name('Role List');
});

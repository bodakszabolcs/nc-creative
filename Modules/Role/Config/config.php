<?php

return [
    'name' => 'Role',
    'menu_order' => 997,
    'menu' => [
        [
            'icon' => 'la la-gear',
            'title' => 'System',
            'route' => '#system',
            'submenu' => [
                [
                    'icon' => 'la la-shield',
                    'title' => 'Roles',
                    'route' => '/'.env('ADMIN_URL').'/role/index'
                ]
            ]
        ]
    ]
];

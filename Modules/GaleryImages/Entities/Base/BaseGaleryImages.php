<?php

namespace Modules\GaleryImages\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;
use Modules\Galery\Entities\Galery;


abstract class BaseGaleryImages extends BaseModel
{


    protected $table = 'galery_images';

    protected $dates = ['created_at', 'updated_at'];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
            'name' => 'image',
            'title' => 'Image',
            'type' => 'text',
        ], [
            'name' => 'title',
            'title' => 'Title',
            'type' => 'text',
        ], [
            'name' => 'description',
            'title' => 'Description',
            'type' => 'text',
        ],];

        return parent::getFilters();
    }


    protected $fillable = ['image', 'galery_id', 'title', 'description'];

    protected $casts = [];


}

<?php

namespace Modules\GaleryImages\Entities;

use Modules\GaleryImages\Entities\Base\BaseGaleryImages;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class GaleryImages extends BaseGaleryImages
{
    use SoftDeletes, Cachable, HasTranslations;


    public $translatable = ["title", "description"];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        return parent::getFilters();
    }

}

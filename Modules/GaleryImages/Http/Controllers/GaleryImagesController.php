<?php

namespace Modules\GaleryImages\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\GaleryImages\Entities\GaleryImages;
use Illuminate\Http\Request;
use Modules\GaleryImages\Http\Requests\GaleryImagesCreateRequest;
use Modules\GaleryImages\Http\Requests\GaleryImagesUpdateRequest;
use Modules\GaleryImages\Transformers\GaleryImagesViewResource;
use Modules\GaleryImages\Transformers\GaleryImagesListResource;

class GaleryImagesController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new GaleryImages();
        $this->viewResource = GaleryImagesViewResource::class;
        $this->listResource = GaleryImagesListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = GaleryImagesCreateRequest::class;
        $this->updateRequest = GaleryImagesUpdateRequest::class;
    }
}

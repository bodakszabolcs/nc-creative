<?php

namespace Modules\GaleryImages\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GaleryImagesCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required',
            'galery_id' => 'required',
           // 'title' => 'required',
           // 'description' => 'required',

        ];
    }

    public function attributes()
    {
        return [
            'image' => __('Image'),
            'galery_id' => __('Galery'),
           // 'title' => __('Title'),
            //'description' => __('Description'),

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

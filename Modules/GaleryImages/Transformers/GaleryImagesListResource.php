<?php

namespace Modules\GaleryImages\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class GaleryImagesListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "image" => $this->image,
            "galery_id" => $this->galery_id,
            "title" => $this->title,
            "description" => $this->description,
        ];
    }
}

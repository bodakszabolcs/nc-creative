<?php

namespace Modules\GaleryImages\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Galery\Entities\Galery;


class GaleryImagesFrontendResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "image" => str_replace('https://','https://cdn'.rand(1,4).'.',env('APP_URL')).$this->image,
            "galery_id" => $this->galery_id,
            "title" => $this->title,
            "description" => $this->description,

        ];
    }
}

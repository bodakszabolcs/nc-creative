<?php

namespace Modules\GaleryImages\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Galery\Entities\Galery;


class GaleryImagesViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "image" => $this->image,
            "galery_id" => $this->galery_id,
            "title" => $this->getTranslatable('title'),
            "description" => $this->getTranslatable('description'),

        ];
    }
}

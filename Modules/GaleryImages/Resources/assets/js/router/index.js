import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import GaleryImages from '../components/GaleryImages'
import GaleryImagesList from '../components/GaleryImagesList'
import GaleryImagesCreate from '../components/GaleryImagesCreate'
import GaleryImagesEdit from '../components/GaleryImagesEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'galery-images',
                component: GaleryImages,
                meta: {
                    title: 'GaleryImages'
                },
                children: [
                    {
                        path: 'index',
                        name: 'GaleryImagesList',
                        component: GaleryImagesList,
                        meta: {
                            title: 'GaleryImages',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/galery-images/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/galery-images/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'GaleryImagesCreate',
                        component: GaleryImagesCreate,
                        meta: {
                            title: 'Create GaleryImages',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/galery-images/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'GaleryImagesEdit',
                        component: GaleryImagesEdit,
                        meta: {
                            title: 'Edit GaleryImages',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/galery-images/index'
                        }
                    }
                ]
            }
        ]
    }
]

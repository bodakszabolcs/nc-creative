<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\GaleryImages\Entities\GaleryImages;

$factory->define(GaleryImages::class, function (Faker $faker) {
    return [
        "image" => $faker->realText(),
        "galery_id" => rand(1000, 5000),
        "title" => $faker->realText(),
        "description" => $faker->realText(),

    ];
});

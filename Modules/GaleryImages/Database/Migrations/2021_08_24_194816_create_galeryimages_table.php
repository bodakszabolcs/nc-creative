<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGaleryImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('galery_images');
        Schema::create('galery_images', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->text("image")->nullable();
            $table->bigInteger("galery_id")->unsigned()->nullable();
            $table->text("title")->nullable();
            $table->text("description")->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galery_images');
    }
}

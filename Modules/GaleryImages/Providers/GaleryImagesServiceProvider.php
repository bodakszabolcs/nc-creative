<?php

namespace Modules\GaleryImages\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\GaleryImages\Entities\GaleryImages;
use Modules\GaleryImages\Observers\GaleryImagesObserver;

class GaleryImagesServiceProvider extends ModuleServiceProvider
{
    protected $module = 'galeryimages';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        GaleryImages::observe(GaleryImagesObserver::class);
    }
}

<?php

namespace Modules\GaleryImages\Observers;

use Modules\GaleryImages\Entities\GaleryImages;

class GaleryImagesObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param \Modules\GaleryImages\Entities\GaleryImages $model
     * @return void
     */
    public function saved(GaleryImages $model)
    {
        GaleryImages::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param \Modules\GaleryImages\Entities\GaleryImages $model
     * @return void
     */
    public function created(GaleryImages $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param \Modules\GaleryImages\Entities\GaleryImages $model
     * @return void
     */
    public function updated(GaleryImages $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param \Modules\GaleryImages\Entities\GaleryImages $model
     * @return void
     */
    public function deleted(GaleryImages $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param \Modules\GaleryImages\Entities\GaleryImages $model
     * @return void
     */
    public function restored(GaleryImages $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param \Modules\GaleryImages\Entities\GaleryImages $model
     * @return void
     */
    public function forceDeleted(GaleryImages $model)
    {
        //
    }
}

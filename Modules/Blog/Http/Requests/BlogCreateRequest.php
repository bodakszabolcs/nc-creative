<?php

namespace Modules\Blog\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title.'.config('app.locale') => 'required',
            'publication_date' => 'required|date'
        ];
    }

    public function attributes()
    {
        return [
            'title.'.config('app.locale') => __('Title'),
            'publication_date' => __('Publication date')
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

<?php

namespace Modules\Blog\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Modules\System\Entities\Settings;

class GeneratePrefixJson implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $shopPrefix = Settings::where('settings_key', 'shop_prefix')->first();
        $productPrefix = Settings::where('settings_key', 'product_prefix')->first();
        $categoryPrefix = Settings::where('settings_key', 'category_prefix')->first();
        $blogPrefix = Settings::where('settings_key', 'blog_prefix')->first();

        $prefixes = [
            'shop' => optional($shopPrefix)->settings_value,
            'product' => optional($productPrefix)->settings_value,
            'category' => optional($categoryPrefix)->settings_value,
            'blog' => optional($blogPrefix)->settings_value
        ];
        Storage::disk(config('filesystems.default_upload_filesystem'))->put('prefixes.json',json_encode($prefixes,JSON_UNESCAPED_UNICODE));
    }
}

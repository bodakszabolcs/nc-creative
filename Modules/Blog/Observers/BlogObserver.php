<?php

namespace Modules\Blog\Observers;

use Modules\Blog\Entities\Blog;

class BlogObserver
{
    /**
     * Handle the blog "creating" event.
     *
     * @param  \Modules\Blog\Entities\Blog  $blog
     * @return void
     */
    public function creating(Blog $blog)
    {
        $blog->user_id = \Auth::id();
    }
}

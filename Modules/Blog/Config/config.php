<?php

return [
    'name' => 'Blog',
    'menu_order' => 18,
    'menu' => [
        [
            'icon' => 'flaticon2-website',
            'title' => 'Slider',
            'route' => '/'.env('ADMIN_URL').'/sliders/index'
        ]
    ]
];

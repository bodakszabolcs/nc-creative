import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Review from '../components/Review'
import ReviewList from '../components/ReviewList'
import ReviewCreate from '../components/ReviewCreate'
import ReviewEdit from '../components/ReviewEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'review',
                component: Review,
                meta: {
                    title: 'Reviews'
                },
                children: [
                    {
                        path: 'index',
                        name: 'ReviewList',
                        component: ReviewList,
                        meta: {
                            title: 'Reviews',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/review/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/review/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'ReviewCreate',
                        component: ReviewCreate,
                        meta: {
                            title: 'Create Reviews',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/review/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'ReviewEdit',
                        component: ReviewEdit,
                        meta: {
                            title: 'Edit Reviews',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/review/index'
                        }
                    }
                ]
            }
        ]
    }
]

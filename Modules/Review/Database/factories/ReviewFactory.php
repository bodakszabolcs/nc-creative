<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Review\Entities\Review;

$factory->define(Review::class, function (Faker $faker) {
    return [
        "image" => $faker->realText(),
"comment" => $faker->realText(),
"title" => $faker->realText(),
"name" => $faker->realText(),
"rating" => rand(1000,5000),

    ];
});

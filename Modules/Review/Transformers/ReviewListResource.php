<?php

namespace Modules\Review\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class ReviewListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "image" => $this->image,
		    "comment" => $this->comment,
		    "title" => $this->title,
		    "name" => $this->name,
		    "rating" => $this->rating,
		     ];
    }
}

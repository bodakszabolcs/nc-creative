<?php

namespace Modules\Review\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Review\Entities\Review;
use Modules\Review\Observers\ReviewObserver;

class ReviewServiceProvider extends ModuleServiceProvider
{
    protected $module = 'review';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Review::observe(ReviewObserver::class);
    }
}

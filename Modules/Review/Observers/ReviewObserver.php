<?php

namespace Modules\Review\Observers;

use Modules\Review\Entities\Review;

class ReviewObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Review\Entities\Review  $model
     * @return void
     */
    public function saved(Review $model)
    {
        Review::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Review\Entities\Review  $model
     * @return void
     */
    public function created(Review $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Review\Entities\Review  $model
     * @return void
     */
    public function updated(Review $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Review\Entities\Review  $model
     * @return void
     */
    public function deleted(Review $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Review\Entities\Review  $model
     * @return void
     */
    public function restored(Review $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Review\Entities\Review  $model
     * @return void
     */
    public function forceDeleted(Review $model)
    {
        //
    }
}

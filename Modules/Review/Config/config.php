<?php

return [
    'name' => 'Review',

                 'menu_order' => 21,

                 'menu' => [
                     [

                      'icon' =>'flaticon-squares',

                      'title' =>'Review',

                      'route' =>'/'.env('ADMIN_URL').'/review/index',

                     ]

                 ]
];

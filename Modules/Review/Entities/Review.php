<?php

namespace Modules\Review\Entities;

use Modules\Review\Entities\Base\BaseReview;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class Review extends BaseReview
{
    use SoftDeletes, Cachable, HasTranslations;


    public $translatable = ["comment", "title"];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }


}

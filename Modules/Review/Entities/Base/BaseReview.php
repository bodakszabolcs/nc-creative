<?php

namespace Modules\Review\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BaseReview extends BaseModel
{


    protected $table = 'reviews';

    protected $dates = ['created_at', 'updated_at'];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
            'name' => 'image',
            'title' => 'Kép',
            'type' => 'text',
        ], [
            'name' => 'comment',
            'title' => 'Komment',
            'type' => 'text',
        ], [
            'name' => 'title',
            'title' => 'Kép címe',
            'type' => 'text',
        ], [
            'name' => 'name',
            'title' => 'Név',
            'type' => 'text',
        ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['image', 'comment', 'title', 'name', 'rating'];

    protected $casts = [];


}

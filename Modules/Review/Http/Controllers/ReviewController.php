<?php

namespace Modules\Review\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Review\Entities\Review;
use Illuminate\Http\Request;
use Modules\Review\Http\Requests\ReviewCreateRequest;
use Modules\Review\Http\Requests\ReviewUpdateRequest;
use Modules\Review\Transformers\ReviewViewResource;
use Modules\Review\Transformers\ReviewListResource;

class ReviewController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Review();
        $this->viewResource = ReviewViewResource::class;
        $this->listResource = ReviewListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = ReviewCreateRequest::class;
        $this->updateRequest = ReviewUpdateRequest::class;
    }

}

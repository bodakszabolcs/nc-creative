<?php

namespace Modules\Review\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReviewCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required',
			'comment' => 'required',
			'title' => 'required',
			'name' => 'required',
			'rating' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'image' => __('Kép'),
'comment' => __('Komment'),
'title' => __('Kép címe'),
'name' => __('Név'),
'rating' => __('Értékelés'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

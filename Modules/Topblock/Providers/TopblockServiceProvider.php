<?php

namespace Modules\Topblock\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Topblock\Entities\Topblock;
use Modules\Topblock\Observers\TopblockObserver;

class TopblockServiceProvider extends ModuleServiceProvider
{
    protected $module = 'topblock';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Topblock::observe(TopblockObserver::class);
    }
}

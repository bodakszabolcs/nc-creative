<?php

namespace Modules\Topblock\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Topblock\Entities\Topblock;
use Illuminate\Http\Request;
use Modules\Topblock\Http\Requests\TopblockCreateRequest;
use Modules\Topblock\Http\Requests\TopblockUpdateRequest;
use Modules\Topblock\Transformers\TopblockViewResource;
use Modules\Topblock\Transformers\TopblockListResource;

class TopblockController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Topblock();
        $this->viewResource = TopblockViewResource::class;
        $this->listResource = TopblockListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = TopblockCreateRequest::class;
        $this->updateRequest = TopblockUpdateRequest::class;
    }

}

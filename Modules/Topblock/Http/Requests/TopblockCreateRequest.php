<?php

namespace Modules\Topblock\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TopblockCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required',
			'title' => 'required',
			'description' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'image' => __('Kép'),
'title' => __('Kép címe'),
'description' => __('Leírás'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

<?php

namespace Modules\Topblock\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class TopblockViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "image" => $this->image,
            "title" => $this->getTranslatable('title'),
            "description" => $this->getTranslatable('description'),
            "selectables" => [
            ]
        ];
    }
}

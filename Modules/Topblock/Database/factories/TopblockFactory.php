<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Topblock\Entities\Topblock;

$factory->define(Topblock::class, function (Faker $faker) {
    return [
        "image" => $faker->realText(),
"title" => $faker->realText(),
"description" => $faker->realText(),

    ];
});

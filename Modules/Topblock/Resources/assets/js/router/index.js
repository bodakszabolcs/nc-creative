import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Topblock from '../components/Topblock'
import TopblockList from '../components/TopblockList'
import TopblockCreate from '../components/TopblockCreate'
import TopblockEdit from '../components/TopblockEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'topblok',
                component: Topblock,
                meta: {
                    title: 'Topblocks'
                },
                children: [
                    {
                        path: 'index',
                        name: 'TopblockList',
                        component: TopblockList,
                        meta: {
                            title: 'Topblocks',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/topblok/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/topblok/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'TopblockCreate',
                        component: TopblockCreate,
                        meta: {
                            title: 'Create Topblocks',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/topblok/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'TopblockEdit',
                        component: TopblockEdit,
                        meta: {
                            title: 'Edit Topblocks',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/topblok/index'
                        }
                    }
                ]
            }
        ]
    }
]

<?php

namespace Modules\Topblock\Entities;

use Modules\Topblock\Entities\Base\BaseTopblock;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class Topblock extends BaseTopblock
{
    use SoftDeletes, Cachable, HasTranslations;


    public $translatable = ["title", "description"];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }


}

<?php

namespace Modules\Topblock\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BaseTopblock extends BaseModel
{
    

    protected $table = 'topbloks';

    protected $dates = ['created_at', 'updated_at'];

    

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'image',
                    'title' => 'Kép',
                    'type' => 'text',
                    ],[
                    'name' => 'title',
                    'title' => 'Kép címe',
                    'type' => 'text',
                    ],[
                    'name' => 'description',
                    'title' => 'Leírás',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['image','title','description'];

    protected $casts = [];

    
}

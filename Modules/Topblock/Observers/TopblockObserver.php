<?php

namespace Modules\Topblock\Observers;

use Modules\Topblock\Entities\Topblock;

class TopblockObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Topblock\Entities\Topblock  $model
     * @return void
     */
    public function saved(Topblock $model)
    {
        Topblock::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Topblock\Entities\Topblock  $model
     * @return void
     */
    public function created(Topblock $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Topblock\Entities\Topblock  $model
     * @return void
     */
    public function updated(Topblock $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Topblock\Entities\Topblock  $model
     * @return void
     */
    public function deleted(Topblock $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Topblock\Entities\Topblock  $model
     * @return void
     */
    public function restored(Topblock $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Topblock\Entities\Topblock  $model
     * @return void
     */
    public function forceDeleted(Topblock $model)
    {
        //
    }
}

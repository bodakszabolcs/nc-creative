<?php

namespace Modules\System\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class SystemNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $title, $icon, $action, $url;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($title, $icon, $action, $url)
    {
        $this->title = $title;
        $this->icon = $icon;
        $this->action = $action;
        $this->url = $url;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => $this->title,
            'icon' => $this->icon,
            'action' => $this->action,
            'url' => $this->url
        ];
    }
}

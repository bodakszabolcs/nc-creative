<?php

namespace Modules\System\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\System\Observers\SettingsObserver;

class SystemServiceProvider extends ModuleServiceProvider
{
    protected $module = 'system';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();
    }

}

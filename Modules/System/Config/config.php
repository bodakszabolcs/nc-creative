<?php

return [
    'name' => 'System',
    'menu_order' => 997,
    'menu' => [
        [
            'icon' => 'la la-gear',
            'title' => 'System',
            'route' => '#system',
            'submenu' => [
                [
                    'icon' => 'la la-files-o',
                    'title' => 'Media',
                    'route' => '/'.env('ADMIN_URL').'/media',
                ],
                [
                    'icon' => 'la la-gears',
                    'title' => 'Settings',
                    'route' => '/'.env('ADMIN_URL').'/system'
                ],
                [
                    'icon' => 'la la-terminal',
                    'title' => 'Console',
                    'route' => '/'.env('ADMIN_URL').'/console'
                ],
                [
                    'icon' => 'la la-globe',
                    'title' => 'Telescope',
                    'route' => '/telescope'
                ]
            ]
        ],
    ]
];

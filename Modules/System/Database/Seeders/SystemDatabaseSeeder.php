<?php

namespace Modules\System\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\System\Entities\Settings;

class SystemDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Settings::create([
            'settings_key' => 'language',
            'settings_value' => json_encode(['hu' => 'magyar','en' => 'english'])
        ]);


    }
}

<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Slider\Entities\Slider;

$factory->define(Slider::class, function (Faker $faker) {
    return [
        "name" => $faker->word(),
        "lead" => $faker->words(4),
        "button_text" => $faker->word(),
        "link" => $faker->realText(),
        "order" => rand(1,10),
        "image_path" => 'https://source.unsplash.com/800x450/?light,clear'.rand(0,9999),

    ];
});

<?php

namespace Modules\Slider\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Modules\Slider\Entities\Slider;
use Illuminate\Http\Request;
use Modules\Slider\Http\Requests\SliderCreateRequest;
use Modules\Slider\Http\Requests\SliderUpdateRequest;
use Modules\Slider\Transformers\SliderViewResource;
use Modules\Slider\Transformers\SliderListResource;

class SliderController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Slider();
        $this->viewResource = SliderViewResource::class;
        $this->listResource = SliderListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = SliderCreateRequest::class;
        $this->updateRequest = SliderUpdateRequest::class;
    }

    public function index(Request $request, $auth = null)
    {
        $filters = $request->input();
        $filters['sort'] = 'order|asc';
        $list = $this->model->searchInModel($filters);

        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }

        if (!is_null($auth)) {
            $list = $list->where('user_id', '=', $auth);
        }

        if (!$this->useResourceAsCollection) {
            return new $this->collection($list);
        }

        return $this->listResource::collection($list)->additional(['filters' => $this->model->getFilters()]);
    }

    public function move(Request $request, $id, $type)
    {
        try {
            $first = Slider::where('id','=',$id)->firstOrFail();
            if ($type == 1) {
                $add = -1;
            } else {
                $add = 1;
            }
            $second = Slider::where('order','=', ($first->order + $add))->firstOrFail();

            $help = $first->order;
            $first->order = $second->order;
            $second->order = $help;
            $first->save();
            $second->save();
        } catch (ModelNotFoundException $e)
        {

        }

        return response()->json(['status' => 'OK'], $this->successStatus);
    }

}

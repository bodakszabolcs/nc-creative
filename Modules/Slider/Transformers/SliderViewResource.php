<?php

namespace Modules\Slider\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class SliderViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->getTranslatable('name'),
		    "lead" => $this->getTranslatable('lead'),
		    "button_text" => $this->getTranslatable('button_text'),
		    "link" => $this->link,
		    "order" => $this->order,
		    "image_path" => $this->image_path,
                "selectables" => [
                ]
		     ];
    }
}

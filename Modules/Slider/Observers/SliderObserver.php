<?php

namespace Modules\Slider\Observers;

use Modules\Slider\Entities\Slider;

class SliderObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Slider\Entities\Slider  $model
     * @return void
     */
    public function saved(Slider $model)
    {
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Slider\Entities\Slider  $model
     * @return void
     */
    public function created(Slider $model)
    {
        $lastSlider = Slider::orderBy('order','DESC')->first();
        $model->order = optional($lastSlider)->order + 1;
        $model->save();
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Slider\Entities\Slider  $model
     * @return void
     */
    public function updated(Slider $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Slider\Entities\Slider  $model
     * @return void
     */
    public function deleted(Slider $model)
    {
        $sliders = Slider::all();
        $order = 1;
        foreach ($sliders as $s) {
            $s->order = $order;
            $s->save();
            $order++;
        }
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Slider\Entities\Slider  $model
     * @return void
     */
    public function restored(Slider $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Slider\Entities\Slider  $model
     * @return void
     */
    public function forceDeleted(Slider $model)
    {
        //
    }
}

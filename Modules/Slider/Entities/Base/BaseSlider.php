<?php

namespace Modules\Slider\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;


abstract class BaseSlider extends BaseModel
{


    protected $table = 'sliders';

    protected $dates = ['created_at', 'updated_at'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [
            [
                'name' => 'name',
                'title' => 'Name',
                'type' => 'text',
            ],
            [
                'name' => 'link',
                'title' => 'Link',
                'type' => 'text',
            ]
        ];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['name','lead','button_text','link', 'order', 'image_path'];

    protected $casts = [];


}

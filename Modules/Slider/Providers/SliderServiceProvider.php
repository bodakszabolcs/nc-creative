<?php

namespace Modules\Slider\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Slider\Entities\Slider;
use Modules\Slider\Observers\SliderObserver;

class SliderServiceProvider extends ModuleServiceProvider
{
    protected $module = 'slider';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Slider::observe(SliderObserver::class);
    }
}

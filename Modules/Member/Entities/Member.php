<?php

namespace Modules\Member\Entities;

use Modules\Member\Entities\Base\BaseMember;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class Member extends BaseMember
{
    use SoftDeletes, Cachable, HasTranslations;


    public $translatable = ["name", "position", "description"];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }


}

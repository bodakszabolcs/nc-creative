<?php

namespace Modules\Member\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
			'position' => 'required',
			'description' => 'required',
			'image' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'name' => __('Name'),
'position' => __('Position'),
'description' => __('description'),
'image' => __('Image'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

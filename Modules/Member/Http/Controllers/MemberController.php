<?php

namespace Modules\Member\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Member\Entities\Member;
use Illuminate\Http\Request;
use Modules\Member\Http\Requests\MemberCreateRequest;
use Modules\Member\Http\Requests\MemberUpdateRequest;
use Modules\Member\Transformers\MemberViewResource;
use Modules\Member\Transformers\MemberListResource;

class MemberController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Member();
        $this->viewResource = MemberViewResource::class;
        $this->listResource = MemberListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = MemberCreateRequest::class;
        $this->updateRequest = MemberUpdateRequest::class;
    }

}

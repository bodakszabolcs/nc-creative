<?php

namespace Modules\Member\Observers;

use Modules\Member\Entities\Member;

class MemberObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Member\Entities\Member  $model
     * @return void
     */
    public function saved(Member $model)
    {
        Member::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Member\Entities\Member  $model
     * @return void
     */
    public function created(Member $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Member\Entities\Member  $model
     * @return void
     */
    public function updated(Member $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Member\Entities\Member  $model
     * @return void
     */
    public function deleted(Member $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Member\Entities\Member  $model
     * @return void
     */
    public function restored(Member $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Member\Entities\Member  $model
     * @return void
     */
    public function forceDeleted(Member $model)
    {
        //
    }
}

import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Member from '../components/Member'
import MemberList from '../components/MemberList'
import MemberCreate from '../components/MemberCreate'
import MemberEdit from '../components/MemberEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'members',
                component: Member,
                meta: {
                    title: 'Members'
                },
                children: [
                    {
                        path: 'index',
                        name: 'MemberList',
                        component: MemberList,
                        meta: {
                            title: 'Members',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/members/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/members/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'MemberCreate',
                        component: MemberCreate,
                        meta: {
                            title: 'Create Members',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/members/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'MemberEdit',
                        component: MemberEdit,
                        meta: {
                            title: 'Edit Members',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/members/index'
                        }
                    }
                ]
            }
        ]
    }
]

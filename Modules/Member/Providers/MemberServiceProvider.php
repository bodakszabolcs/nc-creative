<?php

namespace Modules\Member\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Member\Entities\Member;
use Modules\Member\Observers\MemberObserver;

class MemberServiceProvider extends ModuleServiceProvider
{
    protected $module = 'member';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();
        Member::observe(MemberObserver::class);
    }
}

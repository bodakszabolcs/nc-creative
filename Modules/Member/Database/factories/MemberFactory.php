<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Member\Entities\Member;

$factory->define(Member::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),
"position" => $faker->realText(),
"description" => $faker->realText(),
"image" => $faker->realText(),

    ];
});

<?php

return [
    'name' => 'Member',

                 'menu_order' => 19,

                 'menu' => [
                     [

                      'icon' =>'flaticon-squares',

                      'title' =>'Member',

                      'route' =>'/'.env('ADMIN_URL').'/members/index',

                     ]

                 ]
];

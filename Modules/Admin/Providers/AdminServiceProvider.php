<?php

namespace Modules\Admin\Providers;

use App\Providers\ModuleServiceProvider;

class AdminServiceProvider extends ModuleServiceProvider
{
    protected $module = 'admin';
    protected $directory = __DIR__;

}

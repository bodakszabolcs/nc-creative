import Login from './../components/Login'
import Logout from './../components/Logout'
import Admin from './../components/Admin'
import Dashboard from './../components/Dashboard'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}/login`,
        name: 'Login',
        component: Login,
        meta: {
            title: 'Login Page'
        }
    },
    {
        path: `/${process.env.MIX_ADMIN_URL}/logout`,
        name: 'Logout',
        component: Logout
    },
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                name: 'Dashboard',
                path: '',
                component: Dashboard,
                meta: {
                    title: 'Dashboard',
                    subheader: true
                }
            }
        ]
    }
]

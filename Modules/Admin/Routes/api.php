<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Modules\Admin\Http\Controllers', 'middleware' => ['auth:sanctum']], function () {
    Route::get('/notifications', 'AdminController@notifications');
    Route::get('/menu', 'AdminController@getMenu');

    Route::get('/dashboard', 'AdminController@dashboard')->name('Dashboard');

    Route::post('/upload-file', 'AdminController@uploadFile')->name('Upload file');
});
Route::group(['namespace' => 'Modules\Admin\Http\Controllers', 'middleware' =>[] ], function () {
    Route::get('/review', 'AdminController@review');
    Route::get('/topblock', 'AdminController@block');
    Route::get('/gallery/{slug}', 'AdminController@galeryListBySlug');
    Route::get('/gallery', 'AdminController@galeryList');
    Route::get('/home-gallery', 'AdminController@homeGallery');
    Route::get('/portfolio', 'AdminController@portfolioGallery');
    Route::get('/slides', 'AdminController@getSlides');
    Route::get('/members', 'AdminController@getMembers');
    Route::get('/partners', 'AdminController@getPartners');
    Route::post('/contact', 'AdminController@contact');


});

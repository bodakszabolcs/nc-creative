<?php

namespace Modules\Admin\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Modules\Galery\Entities\Galery;
use Modules\Galery\Transformers\GaleryFrontendResource;
use Modules\Galery\Transformers\GaleryListResource;
use Modules\Member\Entities\Member;
use Modules\Member\Transformers\MemberListResource;
use Modules\Message\Entities\Message;
use Modules\Message\Http\Requests\MessageCreateRequest;
use Modules\Order\Entities\Order;
use Modules\OrderItem\Entities\OrderItem;
use Modules\Partners\Entities\Partners;
use Modules\Partners\Transformers\PartnersListResource;
use Modules\Review\Entities\Review;
use Modules\Review\Transformers\ReviewListResource;
use Modules\Slider\Entities\Slider;
use Modules\Slider\Transformers\SliderListResource;
use Modules\Topblock\Entities\Topblock;
use Modules\Topblock\Transformers\TopblockListResource;
use Modules\User\Entities\User;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;
use Image;

class AdminController extends AbstractLiquidController
{
    public function notifications(Request $request)
    {
        $user = Auth::user();

        return response()->json($user->unreadNotifications, $this->successStatus);
    }

    public function dashboard(Request $request)
    {

        $orders = DB::table('order_status')
            ->leftJoin('orders','order_status.id','=','orders.status')
            ->whereNull('orders.deleted_at')
            ->whereNull('order_status.deleted_at')
            ->select(DB::raw('COUNT(orders.id) as counted,order_status.color as badge'), DB::raw('JSON_EXTRACT(`order_status`.`name`,"$.\"hu\"") as name'), DB::raw('order_status.id as osid'))
            ->groupBy('order_status.id')->orderBy('o','asc')
            ->get();

        $sumOrders = Order::count();
        $tp = [];

            $totalProfit = Order::where('status','=',8)->sum('order_total');

            $tp[1] = price_format($totalProfit, 1);


        $newOrders = Order::where('created_at', '>', now()->add('-1 day'))->count();
        $lastOrders = Order::where('created_at', '>', now()->add('-2 day'))->where('created_at', '<',
            now()->add('-1 day'))->count();

        $newUsers = User::where('created_at', '>', now()->add('-1 day'))->count();
        $lastUsers = User::where('created_at', '>', now()->add('-2 day'))->where('created_at', '<',
            now()->add('-1 day'))->count();




        $chartDataOrders = DB::table('orders')->select(DB::raw('CONCAT(YEAR(created_at)," ",MONTHNAME(created_at)) as Month'),
            DB::raw('COUNT(created_at) as DateCount'))
            ->groupBy([DB::raw('MONTH(created_at)'), DB::raw('MONTHNAME(created_at)')])
            ->orderBy('created_at', 'ASC')
            ->where('created_at', '>', now()->add('- 1 year'))
            ->get()
            ->toArray();

        $chartDataUsers = DB::table('users')->select(DB::raw('CONCAT(YEAR(created_at)," ",MONTHNAME(created_at)) as Month'),
            DB::raw('COUNT(created_at) as DateCount'))
            ->groupBy([DB::raw('MONTH(created_at)'), DB::raw('MONTHNAME(created_at)')])
            ->orderBy('created_at', 'ASC')
            ->where('created_at', '>', now()->add('- 1 year'))
            ->get()
            ->toArray();

        $labels = [];
        foreach ($chartDataOrders as $cd) {
            $labels[] = $cd->Month;
        }

        $co = array_fill(0, sizeof($labels), 0);
        $cu = array_fill(0, sizeof($labels), 0);

        $index = 0;
        foreach ($labels as $l) {
            foreach ($chartDataOrders as $cdo) {
                if ($cdo->Month == $l) {
                    $co[$index] = $cdo->DateCount;
                }
            }
            foreach ($chartDataUsers as $cdo) {
                if ($cdo->Month == $l) {
                    $cu[$index] = $cdo->DateCount;
                }
            }
            $index++;
        }

        $four_stat = [
            'tp' => $tp,
            'new_orders' => $newOrders,
            'change_orders' => ($lastOrders !== 0) ? (round($newOrders / $lastOrders * 100, 2)) : 100,
            'new_users' => $newUsers,
            'change_users' => ($lastUsers !== 0) ? (round($newUsers / $lastUsers * 100, 2)) : 100,
            'stocks' => []
        ];



        $chart = [
            'orders' => $co,
            'users' => $cu,
            'labels' => $labels
        ];


        return response()->json([
            'four_stat' => $four_stat,
            'orders' => $orders,
            'chart' => $chart
        ], $this->successStatus);
    }


    public function getMenu(Request $request)
    {
        $menus = parent::getMenu($request);
        $isSuperAdmin = Auth::user()->isSuperAdmin();

        $superAdminRoutes = ['/' . config('app.admin_url') . '/module-builder/index', '/telescope'];
        $productionRoutes = (config('app.env') === 'production') ? ['/' . config('app.admin_url') . '/module-builder/index'] : [];

        if ($isSuperAdmin) {
            return $menus;
        }

        $mergedRoles = [];
        $roles = Auth::user()->roles;

        foreach ($roles as $r) {
            try {
                $mergedRoles = array_merge($mergedRoles, json_decode($r->menu, true));
            } catch (\Exception $e) {
                $mergedRoles = array_merge($mergedRoles, $r->menu);
            }
        }

        $index = 0;
        foreach ($menus as $m) {
            if (!array_key_exists($m['route'], $mergedRoles)) {
                unset($menus[$index]);
            } else {
                if (in_array($m['route'], $superAdminRoutes) && in_array($m['route'],
                        $productionRoutes) && !$isSuperAdmin) {
                    unset($menus[$index]);
                }
            }

            if (isset($m['submenu'])) {
                $kndex = 0;
                foreach ($m['submenu'] as $k) {
                    if (!array_key_exists($k['route'], $mergedRoles)) {
                        unset($menus[$index]['submenu'][$kndex]);
                    } else {
                        if (in_array($k['route'], $superAdminRoutes) && in_array($k['route'],
                                $productionRoutes) && !$isSuperAdmin) {
                            unset($menus[$index]);
                        }
                    }
                    $kndex++;
                }
            }
            $index++;
        }

        return $menus;
    }

    public function uploadFile(Request $request)
    {

         $file =\Storage::disk('public')->putFileAs('public/files/shares'.$request->input('dir'),$request->file('file'),$request->file('file')->getClientOriginalName());
         $fileExpl = explode("public/", $file);
         $path ='/storage/public/' . end($fileExpl);
         $optimizerChain = OptimizerChainFactory::create();
         $optimizerChain->optimize(public_path($path));

        $filePath = public_path('storage/thumbnails-desktop/'.str_replace($request->file('file')->getClientOriginalName(),'',$file));
        if (!file_exists($filePath)) {
            mkdir($filePath, 0777, true);
        }
        $img = Image::make(public_path($path));
        $img->resize(640, 640, function ($const) {
            $const->aspectRatio();
        })->save($filePath.'/'.$request->file('file')->getClientOriginalName());

        $filePath = public_path('storage/thumbnails-mobile/'.str_replace($request->file('file')->getClientOriginalName(),'',$file));
        if (!file_exists($filePath)) {
            mkdir($filePath, 0777, true);
        }
        $img = Image::make(public_path($path));
        $img->resize(320, 320, function ($const) {
            $const->aspectRatio();
        })->save($filePath.'/'.$request->file('file')->getClientOriginalName());



        return \response()->json(['path' => $path, 'url' => url('storage/' . end($fileExpl))],
            $this->successStatus);
    }
    public function review(Request $request){
        return ReviewListResource::collection(Review::orderBy('id','desc')->get());
    }
    public function block(Request $request){
        return TopblockListResource::collection(Topblock::orderBy('id','desc')->get());
    }
    public function galeryList(Request $request){
        return GaleryListResource::collection(Galery::where('public',1)->get())->additional(['categories'=>config('categories')]);
    }
    public function homeGallery(Request $request){
        return GaleryListResource::collection(Galery::where('public',1)->limit(6)->orderBy('updated_at','desc')->get());
    }
    public function portfolioGallery(Request $request){
        return GaleryListResource::collection(Galery::where('public',2)->orderBy('updated_at','desc')->get());
    }
    public function getSlides(Request $request){
        return SliderListResource::collection(Slider::orderBy('order')->get());
    }
    public function galeryListBySlug(Request $request,$slug){
        $gallery = Galery::whereIn('public',[1,2])->where('slug',$slug)->first();
        if($gallery) {
            return new GaleryFrontendResource($gallery);
        }
        abort(404);
    }
    public function getMembers(Request $request){
        return MemberListResource::collection(Member::all());
    }
    public function getPartners(Request $request){
        return PartnersListResource::collection(Partners::all());
    }

    public function contact(MessageCreateRequest $request){
        $msg = new Message();
        $msg->fill($request->all());
        $msg->save();
        Mail::to('holozsi9@gmail.com')->send(new \App\Mail\Message($msg));
        Mail::to('princeblade88@gmail.com')->send(new \App\Mail\Message($msg));
    }
}

<?php

namespace Modules\ModuleBuilder\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePrefixRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'moduleName' => 'required',
            'prefix' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

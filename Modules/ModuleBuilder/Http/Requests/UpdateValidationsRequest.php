<?php

namespace Modules\ModuleBuilder\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateValidationsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'moduleName' => 'required',
            'validation.*' => 'array',
            'validation.CREATE.*' => 'array',
            'validation.UPDATE.*' => 'array',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

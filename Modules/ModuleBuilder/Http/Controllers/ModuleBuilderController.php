<?php

namespace Modules\ModuleBuilder\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Modules\ModuleBuilder\Http\Requests\ModuleCreateRequest;
use Modules\ModuleBuilder\Jobs\CreateModuleProcess;
use Nwidart\Modules\Facades\Module;

class ModuleBuilderController extends AbstractLiquidController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->createRequest = $this->updateRequest = ModuleCreateRequest::class;
    }

    public function credentials(Request $request)
    {
        $routes = [];
        foreach (Route::getRoutes()->getRoutes() as $route) {
            if (is_array($route->action["middleware"]) && in_array('role', $route->action["middleware"])) {
                $routes[] = $route;
            }
        }

        return response()->json(['routes' => $routes, 'menus' => parent::getMenu($request)], $this->successStatus);
    }

    public function create(Request $request)
    {
        CreateModuleProcess::dispatch(
            $request->input('moduleName'),
            $request->input('newOrExist'),
            $request->input('collection'),
            $request->input('route'),
            $request->input('relations'),
            Auth::user(),
            $request->input('middlewares', []),
            $request->input('options', []),
            $request->input('fields', [])
        );

        return response()->json(['data' => ['message' => 'OK']], $this->successStatus);
    }

    public function getMiddlewares(Request $request)
    {
        return response()->json(['data' => config('modulebuilder.middlewares')], $this->successStatus);
    }

    public function getModels(Request $request)
    {
        $models = [];

        foreach (Module::getByStatus(1) as $m) {
            $x = Storage::disk('modules')->files($m->name . '/Entities');

            foreach ($x as $item) {
                if (strpos($item, '.gitkeep') === false) {
                    $models[] = $item;
                }
            }
        }

        return response()->json(['data' => $models], $this->successStatus);
    }

}

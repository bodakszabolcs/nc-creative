<?php

namespace Modules\ModuleBuilder\Jobs;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\ModuleBuilder\Jobs\Base\BaseModuleProcess;
use Nwidart\Modules\Support\Stub;

class CreateModuleProcess extends BaseModuleProcess
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->createDirectories();
        $this->createJsons();
        $this->createConfig();
        $this->createObserver();
        $this->createServiceProvider();
        $this->createRoutes();
        $this->createRequests();
        $this->createResources();
        $this->createController();
        $this->createEntities();
        $this->createVue();
        $this->createSchema();
        $this->createFactory();
        $this->writeSeeder();
        $this->runMigration();
        $this->fixLintErrors();
    }

    private function createDirectories()
    {
        if (empty($this->newOrExist)) {
            Storage::disk('modules')->makeDirectory($this->moduleName);
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Config');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Console');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Database');
            Storage::disk('modules')->deleteDirectory($this->moduleName . '/Database/Migrations');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Database/Migrations');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Entities');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Http');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Http/Controllers');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Http/Middleware');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Http/Requests');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Transformers');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Providers');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Resources');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Resources/assets');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Resources/assets/js');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Resources/assets/js/components');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Resources/assets/js/router');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Resources/assets/sass');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Tests');
            Storage::disk('modules')->makeDirectory($this->moduleName . '/Routes');
        }
    }

    private function configVue()
    {

        $module = Storage::disk('resources')->get($this->vueModulePath);
        $route = Storage::disk('resources')->get($this->vueRoutePath);
        $moduleData = "require('./../../../Modules/" . $this->moduleName . "/Resources/assets/js/app')";
        if (strpos($module, $moduleData) === false) {
            $module .= "\n" . $moduleData . "\n";
            Storage::disk('resources')->put($this->vueModulePath, $module);
        }

        $matchesRoute = [];
        $newrouteString = 'const routes' . $this->moduleName . ' = require(\'./../../../Modules/' . $this->moduleName . '/Resources/assets/js/router/index\')';
        preg_match_all('/\/\* ROUTE CONFIG START \*\/(.*?)\/\* ROUTE CONFIG END \*\//s', $route, $matchesRoute);
        if (isset($matchesRoute[1][0])) {
            $this->menuOrder = substr_count($matchesRoute[1][0], "\n") + 1;
            if (strpos($matchesRoute[1][0], $newrouteString) === false) {

                $replaceRoute = $matchesRoute[1][0] . $newrouteString . "\n";

                $route = str_replace($matchesRoute[1][0], $replaceRoute, $route);
            }
        }
        $matchesRouteArray = [];
        $newRouteItem = ', routes' . $this->moduleName . '.default] // routesEnd';
        preg_match_all('/\/\* ROUTE ARRAY START \*\/(.*?)\/\* ROUTE ARRAY END \*\//s', $route, $matchesRouteArray);
        if (isset($matchesRouteArray[1][0])) {
            if (strpos($matchesRouteArray[1][0], 'routes' . $this->moduleName . '.default') === false) {

                $replaceRouteItem = str_replace('] // routesEnd', $newRouteItem, $matchesRouteArray[1][0]);

                $route = str_replace($matchesRouteArray[1][0], $replaceRouteItem, $route);
            }
        }
        Storage::disk('resources')->put($this->vueRoutePath, $route);

    }

    private function createObserver()
    {
        $observer = (new Stub($this->stubDirectoryPath . 'observer.stub', [
            'MODULE_NAME' => $this->moduleName
        ]))->render();

        Storage::disk('modules')->put($this->moduleName . '/Observers/' . $this->moduleName . 'Observer.php',
            $observer);
    }

    private function createFactory()
    {
        $factory = (new Stub($this->stubDirectoryPath . 'factory.stub', [
            'MODULE_NAME' => $this->moduleName,
            'FACTORY' => $this->setFactories()
        ]))->render();

        Storage::disk('modules')->put($this->moduleName . '/Database/factories/' . $this->moduleName . 'Factory.php',
            $factory);

        $seeder = Storage::disk('database')->get('seeds/DatabaseSeeder.php');
        $call = '\Modules\\' . $this->moduleName . '\Entities\\' . $this->moduleName . '::unsetEventDispatcher();';

        $call .= "\n\n/* EventDispatcherArea *//* EventDispatcherAreaEnd */";

        $seeder = Str::replaceFirst("/* EventDispatcherArea *//* EventDispatcherAreaEnd */", $call, $seeder);

        Storage::disk('database')->put('seeds/DatabaseSeeder.php', $seeder);

        $seeder = Storage::disk('database')->get('seeds/DatabaseSeeder.php');
        $call = '$model = factory(\Modules\\'.$this->moduleName.'\Entities\\'.$this->moduleName.'::class, 5)->create();';

        $call .= "\n\n/* FakerArea *//* FakerAreaEnd */";

        $seeder = Str::replaceFirst("/* FakerArea *//* FakerAreaEnd */", $call, $seeder);

        Storage::disk('database')->put('seeds/DatabaseSeeder.php', $seeder);
    }

    private function writeSeeder()
    {
        $dbseed = (new Stub($this->stubDirectoryPath . 'seeder.stub', [
            'MODULE_NAME' => $this->moduleName
        ]))->render();

        Storage::disk('modules')->put($this->moduleName . '/Database/Seeders/' . $this->moduleName . 'DatabaseSeeder.php',
            $dbseed);

        $seeder = Storage::disk('database')->get('seeds/DatabaseSeeder.php');
        $call = '$this->call(\Modules\\' . $this->moduleName . '\Database\Seeders\\' . $this->moduleName . 'DatabaseSeeder::class);';

        $call .= "\n\n/* ModuleBuilderSeedArea *//* ModuleBuilderSeedAreaEnd */";

        $seeder = Str::replaceFirst("/* ModuleBuilderSeedArea *//* ModuleBuilderSeedAreaEnd */", $call, $seeder);

        Storage::disk('database')->put('seeds/DatabaseSeeder.php', $seeder);

    }

    private function createJsons()
    {
        $composer = (new Stub($this->stubDirectoryPath . 'composer.stub', [
            'MODULE_NAME' => $this->moduleName,
            'MODULE_NAME_LOWER' => strtolower($this->moduleName),
            'AUTH_NAME' => $this->auth->firstname,
            'AUTH_EMAIL' => $this->auth->email
        ]))->render();

        $module = (new Stub($this->stubDirectoryPath . 'module.stub', [
            'MODULE_NAME' => $this->moduleName,
            'MODULE_NAME_LOWER' => strtolower($this->moduleName),
            'AUTH_NAME' => $this->auth->firstname,
            'AUTH_EMAIL' => $this->auth->email
        ]))->render();
        $this->configVue();

        $webpack = (new Stub($this->stubDirectoryPath . 'webpack.stub', [
            'MODULE_NAME' => $this->moduleName,
            'MODULE_NAME_LOWER' => strtolower($this->moduleName),
            'AUTH_NAME' => $this->auth->firstname,
            'AUTH_EMAIL' => $this->auth->email
        ]))->render();
        $package = (new Stub($this->stubDirectoryPath . 'package.stub', [
            'MODULE_NAME' => $this->moduleName,
            'MODULE_NAME_LOWER' => strtolower($this->moduleName),
            'AUTH_NAME' => $this->auth->firstname,
            'AUTH_EMAIL' => $this->auth->email
        ]))->render();
        Storage::disk('modules')->put($this->moduleName . '/composer.json', $composer);
        Storage::disk('modules')->put($this->moduleName . '/module.json', $module);
        Storage::disk('modules')->put($this->moduleName . '/webpack.mix.js', $webpack);
        Storage::disk('modules')->put($this->moduleName . '/package.json', $package);
    }

    public function createVue()
    {

        $appjs = (new Stub($this->stubDirectoryPath . 'vue-js/js/app.js.stub', [
            'MODULE_NAME' => $this->moduleName,
            'MODULE_NAME_LOWER' => strtolower($this->moduleName),
        ]))->render();
        Storage::disk('modules')->put($this->moduleName . '/Resources/assets/js/app.js', $appjs);

        $routerjs = (new Stub($this->stubDirectoryPath . 'vue-js/js/router/index.js.stub', [
            'MODULE_NAME' => $this->moduleName,
            'MODULE_NAME_PLURAL' => Str::plural($this->moduleName),
            'MODULE_NAME_LOWER' => strtolower($this->moduleName),
            'ROUTE' => ltrim($this->route,'/')
        ]))->render();
        Storage::disk('modules')->put($this->moduleName . '/Resources/assets/js/router/index.js', $routerjs);

        $modeljs = (new Stub($this->stubDirectoryPath . 'vue-js/js/components/Model.vue.stub', [
            'MODULE_NAME' => $this->moduleName,
            'MODULE_NAME_LOWER' => strtolower($this->moduleName),
        ]))->render();
        Storage::disk('modules')->put($this->moduleName . '/Resources/assets/js/components/' . $this->moduleName . '.vue',
            $modeljs);

        $modelCreate = (new Stub($this->stubDirectoryPath . 'vue-js/js/components/ModelCreate.vue.stub', [
            'MODULE_NAME' => $this->moduleName,
            'MODULE_NAME_LOWER' => strtolower($this->moduleName),
            'ROUTE' => ltrim($this->route,'/')
        ]))->render();
        Storage::disk('modules')->put($this->moduleName . '/Resources/assets/js/components/' . $this->moduleName . 'Create.vue',
            $modelCreate);

        $modelEdit = (new Stub($this->stubDirectoryPath . 'vue-js/js/components/ModelEdit.vue.stub', [
            'MODULE_NAME' => $this->moduleName,
            'MODULE_NAME_LOWER' => strtolower($this->moduleName),
            'ROUTE' => ltrim($this->route,'/')
        ]))->render();
        Storage::disk('modules')->put($this->moduleName . '/Resources/assets/js/components/' . $this->moduleName . 'Edit.vue',
            $modelEdit);

        $formElement = "";
        $formImports = "";
        $components = [];

        $hasTranslations = $this->getTranslatable();

        if ($hasTranslations) {
            $formElement .= (new Stub($this->stubDirectoryPath . 'vue-js/js/partials/TranslationComponentStart.stub'))->render();
        }

        foreach ($this->fields as $v) {
            $components[Arr::get($this->componentsArray, $v['column_input'])] = Arr::get($this->componentsArray,
                $v['column_input']);

            $selectable = '{}';

            foreach ($this->relations as $r) {
                if (isset($r['foreignKey']) && $r['foreignKey'] == $v['column_name']) {
                    $mo = explode("/",$r['model']);
                    $mo = end($mo);
                    $mo = str_replace(".php","",$mo);
                    $selectable = 'model.selectables.'.Str::slug($mo);
                    break;
                }
            }

            $formElement .= (new Stub($this->stubDirectoryPath . 'vue-js/vue-form-element/' . $v['column_input'] . '.stub',
                [
                    'NAME' => $v['column_name'] . (($v['has_translation'] == 1) ? '[item]' : ''),
                    'LABEL' => $v['column_label'],
                    'SELECTABLE' => $selectable,
                    'LANG' => ($v['has_translation'] == 1) ? ':lang="item"' : ''
                ]))->render();
            $import = (new Stub($this->stubDirectoryPath . 'vue-js/vue-form-element/imports/' . $v['column_input'] . '.stub',
                [
                    'NAME' => $v['column_name'],
                    'LABEL' => $v['column_label'],
                ]))->render();
            if (strpos($formImports, $import) === false) {
                $formImports .= $import;
            }
        }

        if ($hasTranslations) {
            $formElement .= (new Stub($this->stubDirectoryPath . 'vue-js/js/partials/TranslationComponentEnd.stub'))->render();
        }

        $modelForm = (new Stub($this->stubDirectoryPath . 'vue-js/js/components/ModelForm.vue.stub', [
            'MODULE_NAME' => $this->moduleName,
            'MODULE_NAME_LOWER' => strtolower($this->moduleName),
            'FORM' => $formElement,
            'IMPORTS' => $formImports,
            'COMPONENTS' => implode(', ', $components)
        ]))->render();
        Storage::disk('modules')->put($this->moduleName . '/Resources/assets/js/components/' . $this->moduleName . 'Form.vue',
            $modelForm);

        $modelList = (new Stub($this->stubDirectoryPath . 'vue-js/js/components/ModelList.vue.stub', [
            'MODULE_NAME' => $this->moduleName,
            'MODULE_NAME_LOWER' => strtolower($this->moduleName),
            'FIELDS' => $this->getfields()
        ]))->render();
        Storage::disk('modules')->put($this->moduleName . '/Resources/assets/js/components/' . $this->moduleName . 'List.vue',
            $modelList);
    }

    private function createConfig()
    {

        $config = (new Stub($this->stubDirectoryPath . 'config.stub', [
            'CONFIG' =>
                "'name' => '$this->moduleName'," . "\n
                 'menu_order' => $this->menuOrder,\n
                 'menu' => [
                     [\n
                      'icon' =>'flaticon-squares',\n
                      'title' =>'$this->moduleName',\n
                      'route' =>'/'.env('ADMIN_URL').'/" . str_replace('/', '', $this->route) . "/index',\n
                     ]\n
                 ]",

        ]))->render();

        Storage::disk('modules')->put($this->moduleName . '/Config/config.php', $config);
        $this->config = config(strtolower($this->moduleName));
    }

    private function createServiceProvider()
    {
        $serviceProvider = (new Stub($this->stubDirectoryPath . 'serviceprovider.stub', [
            'MODULE_NAME' => $this->moduleName,
            'MODULE_NAME_LOWER' => strtolower($this->moduleName),
            'AUTH_NAME' => $this->auth->firstname,
            'AUTH_EMAIL' => $this->auth->email
        ]))->render();

        Storage::disk('modules')->put($this->moduleName . '/Providers/' . $this->moduleName . 'ServiceProvider.php',
            $serviceProvider);

    }

    private function createRoutes()
    {
        $routes = (new Stub($this->stubDirectoryPath . 'routes.stub', [
            'MODULE_NAME' => $this->moduleName,
            'MODULE_NAME_LOWER' => strtolower($this->moduleName),
            'ROUTE' => rtrim($this->route,'/')
        ]))->render();

        Storage::disk('modules')->put($this->moduleName . '/Routes/api.php', $routes);

        $this->config['middlewares'] = $this->middlewares;
        $this->config['route'] = str_replace("//", "/", '/' . strtolower($this->route));
    }

    private function createController()
    {

        $controller = (new Stub($this->stubDirectoryPath . 'controller.stub', [
            'MODULE_NAME' => $this->moduleName,
            'DIR' => '',
            'BASE_CONTROLLER_ROUTE' => 'use Modules\\' . $this->moduleName . '\Http\Controllers\Base\Base' . $this->moduleName . 'Controller;',
            'BASE_CONTROLLER' => 'Base' . $this->moduleName . 'Controller',
            'BASE' => ''
        ]))->render();

        Storage::disk('modules')->put($this->moduleName . '/Http/Controllers/' . $this->moduleName . 'Controller.php',
            $controller);

        $this->config['controllers'] = [
            $this->moduleName . '/Http/Controllers/' . $this->moduleName . 'Controller.php'
        ];
    }

    private function createRequests()
    {
        $create = (new Stub($this->stubDirectoryPath . 'request.stub', [
            'MODULE_NAME' => $this->moduleName,
            'TYPE' => 'Create',
            'VALIDATION' => $this->setValidationRules(),
            'ATTRIBUTES' => $this->setSearchLabels()
        ]))->render();

        Storage::disk('modules')->put($this->moduleName . '/Http/Requests/' . $this->moduleName . 'CreateRequest.php',
            $create);

        $update = (new Stub($this->stubDirectoryPath . 'updaterequest.stub', [
            'MODULE_NAME' => $this->moduleName,
            'TYPE' => 'Update',
            'VALIDATION' => $this->setValidationRules(),
            'ATTRIBUTES' => $this->setSearchLabels()
        ]))->render();

        Storage::disk('modules')->put($this->moduleName . '/Http/Requests/' . $this->moduleName . 'UpdateRequest.php',
            $update);

    }

    private function createResources()
    {
        $resource = (new Stub($this->stubDirectoryPath . 'resource.stub', [
            'MODULE_NAME' => $this->moduleName,
            'EXTRA' => 'View',
            'RESOURCE' => $this->getResource(true, true),
            'USESMODEL' => $this->getUsesModel()
        ]))->render();

        Storage::disk('modules')->put($this->moduleName . '/Transformers/' . $this->moduleName . 'ViewResource.php',
            $resource);

        $resource = (new Stub($this->stubDirectoryPath . 'resource.stub', [
            'MODULE_NAME' => $this->moduleName,
            'EXTRA' => 'List',
            'RESOURCE' => $this->getResource(),
            'USESMODEL' => ''
        ]))->render();

        Storage::disk('modules')->put($this->moduleName . '/Transformers/' . $this->moduleName . 'ListResource.php',
            $resource);

        $this->config[$this->moduleName]['resource'] = $this->options['resource'];
        $this->config[$this->moduleName]['show_list'] = $this->options['show_list'];
        $this->config[$this->moduleName]['resources'] = [
            $this->moduleName . '/Transformers/' . $this->moduleName . 'ViewResource.php',
            $this->moduleName . '/Transformers/' . $this->moduleName . 'ListResource.php'
        ];
    }

    private function createEntities()
    {
        $uses = '';

        if (!empty($this->getSoftDeletes())) {
            $uses = $this->getSoftDeletes();
        }

        if (!empty($this->getCachable())) {
            if (empty($uses)) {
                $uses = $this->getCachable();
            } else {
                $uses .= ', ' . $this->getCachable();
            }
        }

        if (!empty($this->getTranslatable())) {
            $uses .= ', ' . $this->getTranslatable();
        }

        $uses = trim($uses, ',');
        $uses = trim($uses);

        if (!empty($uses)) {
            $uses = 'use ' . $uses . ';';
        }

        $relationLoader = 'protected $with = [';

        $innerRelation = '';

        foreach ($this->relations as $c) {
            if (isset($c['relation']) && !empty($c['relation'])) {

                $mo = explode("/",$c['model']);
                $mo = end($mo);
                $mo = str_replace(".php","",$mo);

                $innerRelation .= '"' . Str::slug($mo) . '",';
            }
        }

        $innerRelation = trim($innerRelation, ",");

        $relationLoader .= $innerRelation . '];';

        $model = (new Stub($this->stubDirectoryPath . 'model.stub', [
            'MODULE_NAME' => $this->moduleName,
            'DIR' => '\Base',
            'BASE_MODEL_ROUTE' => 'use App\BaseModel;',
            'BASE_MODEL' => 'BaseModel',
            'RELATION_LOADER' => $relationLoader,
            'BASE' => 'Base',
            'USES' => '',
            'TRANSLATABLE' => '',
            'ASBTRACT' => 'abstract ',
            'DATES' => "protected " . '$dates' . " = ['" . $this->getDates() . "'];",
            'COLLECTION' => "protected " . '$table' . " = '" . $this->collection . "';",
            'FILLABLE' => $this->setFillable(),
            'SEARCH_COLUMNS' => '$this->searchColumns = [' . $this->setSearchColumns() . '];',
            'CASTS' => 'protected $casts = [' . $this->setCasts() . '];',
            'CONNECTION' => $this->setConnections($this->relations),
            'USESMODEL' => $this->getUsesModel()
        ]))->render();

        Storage::disk('modules')->put($this->moduleName . '/Entities/Base/Base' . $this->moduleName . '.php', $model);

        $model = (new Stub($this->stubDirectoryPath . 'model.stub', [
            'MODULE_NAME' => $this->moduleName,
            'DIR' => '',
            'BASE_MODEL_ROUTE' => 'use Modules\\' . $this->moduleName . '\Entities\Base\Base' . $this->moduleName . ';',
            'BASE_MODEL' => 'Base' . $this->moduleName,
            'RELATION_LOADER' => '',
            'BASE' => '',
            'ASBTRACT' => '',
            'USES' => $uses,
            'DATES' => '',
            'COLLECTION' => '',
            'FILLABLE' => '',
            'SEARCH_COLUMNS' => '',
            'CASTS' => '',
            'CONNECTION' => '',
            'USESMODEL' => '',
            'TRANSLATABLE' => ($this->getTranslatableFields()) ? 'public $translatable = [' . $this->getTranslatableFields() . '];' : '',
        ]))->render();

        Storage::disk('modules')->put($this->moduleName . '/Entities/' . $this->moduleName . '.php', $model);
    }

    private function createSchema()
    {
        $softdelete = '';
        if (isset($this->options['softdeletes']) && $this->options['softdeletes'] == 1) {
            $softdelete = '$table->softDeletes();';
        }

        $migrationFields = '';

        $schematicHelper = [];
        $schematicHelper['softdelete'] = (isset($this->options['softdelete'])) ? $this->options['softdelete'] : null;
        $helper = [
            'TEXT' => 'text',
            'LONGTEXT' => 'longText',
            'INT' => 'integer',
            'BIGINT' => 'bigInteger',
            'TINYINT' => 'tinyInteger',
            'FLOAT' => 'float',
            'VARCHAR' => 'string',
            'DATE' => 'date',
            'DATETIME' => 'timestamp',
            'JSON' => 'json'

        ];

        $inner = '';
        $fks = '';

        foreach ($this->fields as $field) {

            if (!is_null(Arr::get($field, 'column_type', null))) {
                $migrationFields .= '$table->' . Arr::get($helper, $field['column_type'],
                        'string') . '("' . Arr::get($field, 'column_name', null) . '")' .
                    (in_array(Arr::get($helper, $field['column_type'], 'string'),
                        ['integer', 'bigInteger', 'tinyInteger']) ? '->unsigned()->nullable()' : '->nullable()')
                    . ';' . "\n";

            }

            foreach ($this->relations as $relation) {
                if (!is_null(Arr::get($relation, 'foreignKey', null))) {
                    if (Arr::get($field,
                            'column_name') == $relation['foreignKey'] && $relation['relation'] == 'belongsto') {
                        $inner .= '"' . Arr::get($field, 'column_name') . '",';

                        $modelClass = '\Modules\\' . str_replace(['/', '.php'], ['\\', ''], $relation['model']);

                        $model = new $modelClass;

                        $fks .= '$table->foreign("' . Arr::get($field,
                                'column_name') . '")->references("id")->on("' . $model->getTable() . '");' . "\n";
                    }
                }
            }
        }

        $indexes = '$table->index([';

        if (!empty($softdelete)) {
            $inner .= '"deleted_at",';
        }

        $indexes .= trim($inner, ",");
        $indexes .= ']);';

        $migration = (new Stub($this->stubDirectoryPath . 'migration.stub', [
            'MODULE_NAME' => $this->moduleName,
            'COLLECTION' => $this->collection,
            'INCREMENTS' => '$table->bigIncrements("id");',
            'MIGRATION' => $migrationFields,
            'TIMESTAMPS' => '$table->timestamps();',
            'SOFTDELETES' => $softdelete,
            'INDEXES' => $indexes,
            'FOREIGN' => $fks
        ]))->render();

        $slug = date("Y") . '_' . date("m") . "_" . date("d") . "_" . date("His") . "_create_" . Str::slug($this->moduleName) . "_table.php";

        Storage::disk('modules')->put($this->moduleName . '/Database/Migrations/' . $slug, $migration);

        $this->config[$this->moduleName]['schema'][] = $this->moduleName . '/Database/Migrations/' . $slug;
        $this->config[$this->moduleName]['schematic_helper'][] = $schematicHelper;
    }
}

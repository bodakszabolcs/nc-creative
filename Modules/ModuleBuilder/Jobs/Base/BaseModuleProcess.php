<?php

namespace Modules\ModuleBuilder\Jobs\Base;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;
use Nwidart\Modules\Support\Stub;

class BaseModuleProcess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $moduleName;
    protected $newOrExist;
    protected $collection;
    protected $route;
    protected $stubDirectoryPath = '/../../../../../../Modules/ModuleBuilder/Resources/stubs/';
    //resource directory
    protected $vueModulePath = 'js/admin/modules.js';
    protected $vueRoutePath = 'js/admin/routes.js';
    protected $menuOrder = 100;
    protected $config;
    protected $relations;
    protected $auth;
    protected $middlewares;
    protected $options;
    protected $fields;
    protected $configString;
    protected $componentsArray = [
        'input' => 'FormInput',
        'date' => 'FormDatePicker',
        'datetime' => 'FormDateTimePicker',
        'select' => 'FormSelect',
        'file' => 'FormFileUpload',
        'checkbox' => 'FormCheckbox',
        'radio' => 'FormRadio',
        'ckeditor' => 'FormWyswygEditor',
        'textarea' => 'FormTextArea',
    ];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        $moduleName,
        $newOrExist,
        $collection,
        $route,
        $relations,
        $auth = null,
        array $middlewares = ['auth:sanctum', 'acl', 'accesslog'],
        array $options = [
            'softdeletes' => 1,
            'modelCache' => 1,
            'timestamps' => ['created_at', 'updated_at', 'deleted_at'],
        ],
        array $fields = []
    ) {
        $this->moduleName = ucfirst($moduleName);

        $this->collection = $collection;
        $this->fields = $fields;
        if (!is_null($auth)) {
            $this->auth = $auth;
        } else {
            $this->auth = \Modules\User\Entities\User::first();
        }
        $this->route = $route;
        $this->newOrExist = $newOrExist;
        $this->middlewares = $middlewares;
        $this->relations = $relations;
        $this->options = $this->setOptions($options);
    }

    protected function getFields()
    {
        $fields = '';

        $length = sizeof($this->fields);
        $index = 0;

        foreach ($this->fields as $k => $v) {
            if ($v['show_in_list'] == 1) {

                if ($index != 0) {
                    $fields .= "                ";
                }
                $fields .= "{
                    name: '" . $v['column_name'] . "',
                    sortField: '" . $v['column_name'] . "',
                    title: this.".'$translate'.".t('" . $v['column_label'] . "')
                },";

                if ($index != $length -1) {
                    $fields .= "\n";
                }
            }
            $index++;
        }

        return $fields;
    }

    protected function setFactories()
    {
        $factory = '';

        foreach ($this->fields as $k => $v) {
            switch ($v['column_type']) {
                case 'INT':
                case 'BIGINT':
                case 'FLOAT':
                    $type = 'rand(1000,5000)';
                    break;
                case 'TINYINT':
                    $type = 'rand(1,10)';
                    break;
                case 'DATE':
                    $type = '$faker->date()';
                    break;
                case 'DATETIME':
                    $type = '$faker->dateTime()';
                    break;
                case 'TEXT';
                case 'JSON';
                case 'LONGTEXT';
                case 'VARCHAR(255)':
                default:
                    $type = '$faker->realText()';
                    break;
            }
            $factory .= '"' . $v['column_name'] . '" => ' . $type . ",\n";
        }

        return $factory;
    }

    protected function runMigration()
    {
        Artisan::call('migrate --path="Modules/' . $this->moduleName . '/Database/Migrations"');
    }

    protected function setOptions($options)
    {
        $this->options = $options;
        if (!isset($options['timestamp'])) {
            $this->options['timestamp'] = [];
        }
        if (!isset($options['softdeletes'])) {
            $this->options['softdeletes'] = 1;
        }

        return $this->options;
    }

    protected function getUsesModel()
    {
        $uses = '';
        foreach ($this->relations as $r) {
            if (isset($r['relation']) && !empty($r['relation'])) {
                $mo = explode(".php", $r['model']);
                $mo = 'Modules\\'.$mo[0];
                $mo = str_replace("/","\\",$mo);

                $uses .= 'use '.$mo.';'."\n\t\t";

            }
        }

        return $uses;
    }

    protected function setConnections(&$connections)
    {

        $connectionString = '';
        if (is_array($connections)) {
            foreach ($connections as $c) {
                if (isset($c['relation']) && !empty($c['relation'])) {

                    $mo = explode("/",$c['model']);
                    $mo = end($mo);
                    $mo = str_replace(".php","",$mo);

                    $model = (new Stub($this->stubDirectoryPath . 'model-connections/' . $c['relation'] . '.stub', [
                        'CONNECTION_NAME' => Str::slug($mo),
                        'MODEL' => 'Modules\\' . str_replace(['/', '.php'], ['\\', ''], $c['model']),
                        'KEY1' => (isset($c['foreignKey']) && !empty($c['foreignKey'])) ? $c['foreignKey'] : null,
                        'KEY2' => (isset($c['relatedKey']) && !empty($c['relatedKey'])) ? $c['relatedKey'] : null,
                        'TABLE' => isset($c['table']) ? $c['table'] : null,
                    ]))->render();

                    $connectionString .= $model . "\n\n";
                }
            }
        }

        return $connectionString;
    }

    protected function setValidationRules()
    {

        $rulesString = '';
        foreach ($this->fields as $k => $v) {
            if ($v['required'] == 1) {
                $rulesString .= "'" . $v['column_name'] . "' => 'required',\n\t\t\t";
            }
        }

        return $rulesString;
    }

    protected function setFillable()
    {
        $fillabel = [];
        foreach ($this->fields as $k => $v) {
            if ($v['column_fillable'] == 1) {
                $fillabel[] = $v['column_name'];
            }
        }
        if (!empty($fillabel)) {
            return "protected " . '$fillable' . " = ['" . implode("','", $fillabel) . "'];";
        }
    }

    protected function setSearchColumns()
    {
        $search = "";
        foreach ($this->fields as $k => $v) {

            switch ($v['column_type']) {
                case 'DATE':
                case 'DATETIME':
                    $type = 'date_interval';
                    break;
                default:
                    $type = 'text';
                    break;
            }

            switch ($v['column_input']) {
                case 'select':
                    $type = 'select';
                    break;
                default:
                    $type = 'text';
                    break;
            }

            if ($v['column_search'] == 1) {
                if ($type == 'select') {
                    foreach ($this->relations as $r) {
                        if ($r['foreignKey'] == $v['column_name']) {
                            $mo = explode(".php", $r['model']);
                            $mo = 'Modules\\'.$mo[0];
                            $mo = str_replace("/","\\",$mo);
                            $mo = explode('\\',$mo);
                            $mo = end($mo);

                            $search .= "[
                            'name' => '" . $v['column_name'] . "',
                            'title' => '" . $v['column_label'] . "',
                            'type' => '" . $type . "',
                            'data' => ".$mo."::pluck('name','id')
                            ],";

                            break;
                        }
                    }
                } else {
                    $search .= "[
                    'name' => '" . $v['column_name'] . "',
                    'title' => '" . $v['column_label'] . "',
                    'type' => '" . $type . "',
                    ],";
                }
            }
        }
        if (!empty($search)) {
            return $search;
        }
    }

    protected function setCasts()
    {
        $search = "";
        foreach ($this->fields as $k => $v) {
            if ($v['column_cast'] == 1) {
                $search .= "'" . $v['column_name'] . "' => 'array',";
            }
        }
        if (!empty($search)) {
            return $search;
        }
    }

    protected function setSearchLabels()
    {
        $search = "";
        foreach ($this->fields as $k => $v) {
            $search .= "'" . $v['column_name'] . "' => __('" . $v['column_label'] . "'),\n";
        }
        if (!empty($search)) {
            return $search;
        }
    }

    protected function getSoftDeletes()
    {
        if ($this->options['softdeletes'] == 1) {
            return 'SoftDeletes';
        }
        return '';
    }

    protected function getTranslatable()
    {
        $fillabel = '';
        foreach ($this->fields as $k => $v) {
            if ($v['has_translation'] == 1) {
                $fillabel = 'HasTranslation';
            }
        }

        return $fillabel;
    }

    protected function isTranslatable($name) {
        $fillabel = false;
        foreach ($this->fields as $k => $v) {
            if ($v['column_name'] == $name && $v['has_translation'] == 1) {
                $fillabel = true;
            }
        }

        return $fillabel;
    }

    protected function getTranslatableFields() {
        $fillabel = '';
        foreach ($this->fields as $k => $v) {
            if ($v['has_translation'] == 1) {
                $fillabel .= '"'.$v['column_name'].'",';
            }
        }

        return trim($fillabel, ',');
    }

    protected function getCachable()
    {
        if ($this->options['modelCache'] == 1) {
            return 'Cachable';
        }
        return '';
    }

    protected function getResource($connections = false, $view = false)
    {
        $resourceString = '[' . "\n";
        $resourceString .= '            "' . 'id' . '" => $this->' . 'id' . ",\n\t\t";

        foreach ($this->fields as $k => $v) {
            if ($view) {
                if ($v['has_translation'] == 1) {
                    $resourceString .= '    "' . $v['column_name'] . '" => $this->getTranslatable(' . $v['column_name'] . "),\n\t\t";
                } else {
                    $resourceString .= '    "' . $v['column_name'] . '" => $this->' . $v['column_name'] . ",\n\t\t";
                }
            } else {
                $resourceString .= '    "' . $v['column_name'] . '" => $this->' . $v['column_name'] . ",\n\t\t";
            }
        }

        if ($connections) {
            $resourceString .= '"selectables" => ['."\n\t\t";

            foreach ($this->relations as $c) {
                if (isset($c['relation']) && !empty($c['relation'])) {

                    $mo = explode(".php", $c['model']);
                    $mo = 'Modules\\'.$mo[0];
                    $mo = str_replace("/","\\",$mo);
                    $mo = explode('\\',$mo);
                    $mo = end($mo);

                    $resourceString .= '"'.Str::slug($mo).'" => '.$mo.'::pluck("name","id")'.",\n\t\t";
                }
            }

            $resourceString .= "]\n\t\t";
        }

        return $resourceString . "     ]";

    }

    protected function getDates()
    {
        if (!empty($this->options['timestamps'])) {
            return implode("', '", $this->options['timestamps']);
        }

        return "created_at','updated_at','deleted_at";
    }

    protected function fixLintErrors()
    {
        //TODO: Majd valamit ki kell találni ide
    }
}

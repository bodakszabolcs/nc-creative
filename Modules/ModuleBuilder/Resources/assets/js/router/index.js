import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'

import ModuleBuilderCreate from '../components/ModuleBuilderCreate'
import ModuleBuilder from '../components/ModuleBuilder'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'module-builder',
                component: ModuleBuilder,
                meta: {
                    title: 'Roles'
                },
                children: [

                    {
                        path: 'index',
                        name: 'ModuleBuilderCreate',
                        component: ModuleBuilderCreate,
                        meta: {
                            title: 'Create Module',
                            subheader: true

                        }
                    }

                ]
            }
        ]
    }
]

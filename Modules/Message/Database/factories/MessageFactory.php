<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Message\Entities\Message;

$factory->define(Message::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),
"email" => $faker->realText(),
"phone" => $faker->realText(),
"message" => $faker->realText(),
"status" => $faker->randomNumberBetween(1000,5000),

    ];
});

<?php

namespace Modules\Message\Observers;

use Modules\Message\Entities\Message;

class MessageObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Message\Entities\Message  $model
     * @return void
     */
    public function saved(Message $model)
    {
        Message::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Message\Entities\Message  $model
     * @return void
     */
    public function created(Message $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Message\Entities\Message  $model
     * @return void
     */
    public function updated(Message $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Message\Entities\Message  $model
     * @return void
     */
    public function deleted(Message $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Message\Entities\Message  $model
     * @return void
     */
    public function restored(Message $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Message\Entities\Message  $model
     * @return void
     */
    public function forceDeleted(Message $model)
    {
        //
    }
}

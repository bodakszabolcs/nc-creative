<?php

namespace Modules\Message\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Message\Entities\Message;
use Modules\Message\Observers\MessageObserver;

class MessageServiceProvider extends ModuleServiceProvider
{
    protected $module = 'message';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Message::observe(MessageObserver::class);
    }
}

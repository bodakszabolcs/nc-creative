<?php

return [
    'name' => 'Message',

    'menu_order' => 18,

    'menu' => [
        [

            'icon' => 'la la-envelope-o',

            'title' => 'Message',

            'route' => '/' . env('ADMIN_URL') . '/message/index',

        ]

    ]
];

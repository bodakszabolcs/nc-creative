<?php

    namespace Modules\Message\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;
    class SendReplyRequest extends FormRequest
    {
        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'content' => 'required', 'id' => 'required', 'subject' => 'required',
            ];
        }

        public function attributes()
        {
            return [
                'subject' => __('Subject'), 'content' => __('Content'),
            ];
        }

        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    }

<?php

namespace Modules\Message\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class MessageListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "email" => $this->email,
		    "phone" => $this->phone,
		    "message" => $this->message,
		    "status" => $this->status,
		     ];
    }
}

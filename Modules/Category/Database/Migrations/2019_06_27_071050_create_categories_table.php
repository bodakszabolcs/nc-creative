<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);

        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->json('name');
            $table->string('slug')->nullable();
            $table->json('meta_title')->nullable();
            $table->json('meta_description')->nullable();
            $table->string('og_image')->nullable();
            $table->bigInteger('parent_id')->default(null)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['deleted_at']);
        });

        Schema::create('tags', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->json('name');
            $table->string('slug')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['deleted_at']);
        });

        Schema::create('blog_categories', function (Blueprint $table) {
            $table->bigInteger('category_id')->unsigned();
            $table->bigInteger('blog_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('blog_id')->references('id')->on('blog');
            $table->index(['category_id', 'blog_id']);
        });

        Schema::create('blog_tags', function (Blueprint $table) {
            $table->bigInteger('tags_id')->unsigned();
            $table->bigInteger('blog_id')->unsigned();
            $table->foreign('tags_id')->references('id')->on('tags');
            $table->foreign('blog_id')->references('id')->on('blog');
            $table->index(['tags_id', 'blog_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
        Schema::drop('tags');
        Schema::drop('blog_categories');
        Schema::drop('blog_tags');
    }
}

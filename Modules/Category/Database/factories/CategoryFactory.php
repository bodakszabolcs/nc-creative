<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Category\Entities\Category;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->realText(10),
        'slug' => $faker->unique()->slug,
        'meta_title' => $faker->unique()->realText(10),
        'meta_description' => $faker->unique()->realText(100),
        'og_image' => 'https://source.unsplash.com/800x450/?nature,city'.rand(0,9999),
        'parent_id' => function () {
            if (rand(1,2) == 1) {
                return factory(Category::class)->create()->id;
            }

            return null;
        }
    ];
});

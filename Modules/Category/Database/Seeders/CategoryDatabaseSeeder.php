<?php

namespace Modules\Category\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\System\Entities\Settings;
use Modules\Blog\Jobs\GeneratePrefixJson;

class CategoryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Settings::firstOrCreate(['settings_key' => 'category_prefix', 'settings_value' => 'blog']);

        GeneratePrefixJson::dispatch();
    }
}

<?php

namespace Modules\Category\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name.'.config('app.locale') => 'required|unique_translation:categories,name,'.$this->id.',id,deleted_at,NULL',
            'slug' => 'unique:categories,slug,'.$this->id.',id,deleted_at,NULL',
        ];
    }

    public function attributes()
    {
        return [
            'name.'.config('app.locale') => __('Name'),
            'slug' => __('URL')
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

<?php

namespace Modules\Category\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Category\Entities\Category;

class CategoryServiceProvider extends ModuleServiceProvider
{
    protected $module = 'category';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();
    }

}

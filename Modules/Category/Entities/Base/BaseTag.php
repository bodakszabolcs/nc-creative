<?php

namespace Modules\Category\Entities\Base;

use App\BaseModel;

abstract class BaseTag extends BaseModel
{

    protected $dates = ['deleted_at'];

    protected $table = 'tags';

    protected $fillable = [
        'name',
        'slug'
    ];

}

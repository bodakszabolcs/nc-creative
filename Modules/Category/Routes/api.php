<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace' => 'Modules\Category\Http\Controllers',
    'prefix' => '/tag',
    'middleware' => ['auth:sanctum', 'role', 'accesslog']
], function () {
    Route::match(['get'], '/index', 'TagController@index')->name('Tag list');
    Route::match(['get'], '/show/{id?}', 'TagController@show')->name('Tag show');
    Route::match(['post'], '/create', 'TagController@create')->name('Tag create');
    Route::match(['put'], '/update/{id}', 'TagController@update')->name('Tag update');
    Route::match(['delete'], '/delete/{id}', 'TagController@destroy')->name('Tag delete');
});

Route::group([
    'namespace' => 'Modules\Category\Http\Controllers',
    'prefix' => '/category',
    'middleware' => ['auth:sanctum']
], function () {
    Route::match(['get'], '/all', 'CategoryController@getCategories')->name('Get all categories');
});

Route::group([
    'namespace' => 'Modules\Category\Http\Controllers',
    'prefix' => '/category',
    'middleware' => ['auth:sanctum', 'role', 'accesslog']
], function () {
    Route::match(['get'], '/tree', 'CategoryController@getCategoryTree')->name('Get categories tree');
});

Route::group([
    'namespace' => 'Modules\Category\Http\Controllers',
    'prefix' => '/tag',
    'middleware' => ['auth:sanctum']
], function () {
    Route::match(['get'], '/all', 'TagController@getTags')->name('Get all tags');
});

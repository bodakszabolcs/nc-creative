<?php

namespace Modules\Translation\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Translation\Console\GenerateTranslation;

class TranslationServiceProvider extends ModuleServiceProvider
{
    protected $module = 'translation';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();
        $this->commands([
            GenerateTranslation::class
        ]);
    }
}

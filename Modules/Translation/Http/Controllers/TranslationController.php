<?php

namespace Modules\Translation\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Http\Request;
use Modules\System\Entities\Settings;

class TranslationController extends AbstractLiquidController
{
    public function translationList(Request $request)
    {
        $trans = Settings::where('settings_key', '=', 'language')->first();
        $translations = [];

        foreach (json_decode($trans->settings_value) as $key => $val) {
            $translations[$key] = json_decode(file_get_contents(resource_path('lang/' . $key . '.json')));
        }

        return response()->json([
            'languages' => $translations
        ]);
    }

    public function translationSave(Request $request)
    {
        if (!empty($request->input('lang')) && !empty($request->input('key'))) {
            if (file_exists(resource_path('lang/' . $request->input('lang') . '.json'))) {
                $data = file_get_contents(resource_path('lang/' . $request->input('lang') . '.json'));

                $avilable = json_decode($data, true);

                if (is_array($avilable)) {

                    $avilable[$request->input('key')] = $request->input('value');
                    $temp = json_encode($avilable, JSON_UNESCAPED_UNICODE);
                    file_put_contents(resource_path('lang/' . $request->input('lang') . '.json'), $temp);
                    return response()->json($avilable, $this->successStatus);
                }

            }

        }
        return response()->json('error', $this->errorStatus);
    }

    public function translationDelete(Request $request)
    {
        if (!empty($request->input('lang')) && !empty($request->input('key'))) {
            if (file_exists(resource_path('lang/' . $request->input('lang') . '.json'))) {
                $data = file_get_contents(resource_path('lang/' . $request->input('lang') . '.json'));

                $avilable = json_decode($data, true);

                if (is_array($avilable)) {
                    unset($avilable[$request->input('key')]);
                    $temp = json_encode($avilable, JSON_UNESCAPED_UNICODE);
                    file_put_contents(resource_path('lang/' . $request->input('lang') . '.json'), $temp);
                    return response()->json($avilable, $this->successStatus);
                }

            }

        }
        return response()->json('error', $this->errorStatus);
    }

    public function translationGet(Request $request, $lang = null)
    {
        if ($lang != null) {
            $json = file_get_contents(resource_path('lang/' . $lang . '.json'));

            return \response()->json(json_decode($json), $this->successStatus);
        } else {
            $trans = Settings::where('settings_key', '=', 'language')->first();
            $translations = [];

            foreach (json_decode($trans->settings_value) as $key => $val) {
                $translations[$key] = json_decode(file_get_contents(resource_path('lang/' . $key . '.json')));
            }

            return response()->json([
                'languages' => $translations
            ]);
        }
    }
}

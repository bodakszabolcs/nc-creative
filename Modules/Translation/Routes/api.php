<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace' => 'Modules\Translation\Http\Controllers',
    'prefix' => '/translation',
    'middleware' => ['auth:sanctum', 'role']
], function () {
    Route::match(['get'], '/list', 'TranslationController@translationList')->name('Translation list');
});

Route::group([
    'namespace' => 'Modules\Translation\Http\Controllers',
    'prefix' => '/translation',
    'middleware' => ['auth:sanctum', 'role', 'accesslog']
], function () {
    Route::match(['post'], '/save', 'TranslationController@translationSave')->name('Translation save');
    Route::match(['delete'], '/delete', 'TranslationController@translationDelete')->name('Translation delete');
});

Route::group([
    'namespace' => 'Modules\Translation\Http\Controllers',
    'prefix' => '/translation'
], function () {
    Route::get('/{lang?}', 'TranslationController@translationGet');
});

<?php

return [
    'name' => 'Translation',
    'menu_order' => 50,
    'menu' => [
        [
            'icon' => 'la la-gear',
            'title' => 'System',
            'route' => '#system',
            'submenu' => [
                [
                    'icon' => 'la la-list',
                    'title' => 'Translations',
                    'route' => '/'.env('ADMIN_URL').'/translations',
                ]
            ]
        ]
    ]
];

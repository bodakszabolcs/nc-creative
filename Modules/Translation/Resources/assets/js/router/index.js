import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Translation from '../components/Translation'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'translations',
                component: Translation,
                meta: {
                    title: 'Translations',
                    subheader: true
                }
            }
        ]
    }
]

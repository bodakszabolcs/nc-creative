<?php

namespace Modules\Translation\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Menu\Entities\Menu;
use Modules\System\Entities\Settings;
use Nwidart\Modules\Support\Stub;

class GenerateTranslation extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */

    protected $signature = 'generate:translations {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scan files and group translations to the correct place.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $force = $this->option('force');
        $translations = [];
        $allFiles = [];
        foreach (Storage::disk('app')->allFiles() as $file) {
            $allFiles[] = app_path($file);
        }
        foreach (Storage::disk('resources')->allFiles() as $file) {
            $allFiles[] = resource_path($file);
        }
        foreach (Storage::disk('modules')->allFiles() as $file) {
            $allFiles[] = base_path('Modules/' . $file);
        }

        $routes = $this->getRoutes();

        $menuItems = [];
        $this->collectMenuTitles(Menu::getMenu(), $menuItems);

        $bar = $this->output->createProgressBar(count($allFiles) + count($routes) + count($menuItems));
        $bar->start();

        foreach ($allFiles as $af) {

            $this->line(' ' . $af);

            if (!Str::endsWith($af, ['.DS_Store', '.gitkeep', '.gitignore', '.json', '.png', '.jpg', '.jpeg', '.ai', '.psd', '.stub']) && !Str::contains($af, ['.scannerwork'])) {

                preg_match_all('/\__\(\'(.*?)\'\)|\__\(\'(.*?)]\)||\__\(\"(.*?)]\)|\__\("(.*?)"\)|\$t\(\'(.*?)\'\)|this.\$i18n.t\(\'(.*?)\'\)|\$t\("(.*?)"\)|this.\$i18n.t\("(.*?)"\)|self.\$i18n.t\("(.*?)"\)|self.\$i18n.t\("\'(.*?)\'\)/',
                    file_get_contents($af), $output_array);

                for ($i = 1; $i < sizeof($output_array); $i++) {
                    foreach ($output_array[$i] as $val) {
                        if (!empty($val)) {
                            if (Str::contains($val, '[')) {
                                $valHelper = explode("',", $val);
                                $v = str_replace('\\','',$valHelper[0]);
                                $translations[trim($v)] = trim($v);
                            } else {
                                $val = str_replace('\\','',$val);
                                $translations[trim($val)] = trim($val);
                            }
                        }
                    }
                }
            }

            $bar->advance();
        }

        foreach ($routes as $rt) {
            $this->line(' Route: ' . $rt->action['as']);

            if (isset($rt->action['as']) && !empty($rt->action['as'])) {
                $translations[trim($rt->action['as'])] = trim($rt->action['as']);
            }

            $bar->advance();
        }

        foreach ($menuItems as $mi) {
            $this->line(' Menu: ' . $mi);

            $translations[trim($mi)] = trim($mi);

            $bar->advance();
        }

        $trans = Settings::where('settings_key', '=', 'language')->first();


        foreach (json_decode($trans->settings_value) as $key => $val) {

            if (!Storage::disk('resources')->exists('lang/' . $key)) {
                Storage::disk('resources')->createDir('lang/' . $key);

                if ($key !== 'en') {
                    $validation = file_get_contents('https://raw.githubusercontent.com/caouecs/Laravel-lang/master/src/' . $key . '/validation.php');

                    $validation = preg_replace('/\'attributes\' => \[.*\s*\'.*\s*\'.*\s.*\],/m',
                        "'attributes' => json_decode(file_get_contents(resource_path('/lang/$key.json')), true),\n",
                        $validation);

                    $auth = file_get_contents('https://raw.githubusercontent.com/caouecs/Laravel-lang/master/src/' . $key . '/auth.php');

                    $passwords = file_get_contents('https://raw.githubusercontent.com/caouecs/Laravel-lang/master/src/' . $key . '/passwords.php');

                    Storage::disk('resources')->put('lang/' . $key . '/validation.php',
                        $validation);
                    Storage::disk('resources')->put('lang/' . $key . '/auth.php',
                        $auth);
                    Storage::disk('resources')->put('lang/' . $key . '/passwords.php',
                        $passwords);
                }
            }

            if (File::exists(resource_path('lang/' . $key . '.json')) && !$force) {
                $language = json_decode(file_get_contents(resource_path('lang/' . $key . '.json')), 1);

                $language = array_merge($language, $translations);
            } else {
                $language = $translations;
            }

            $lng = json_encode($language, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);

            Storage::disk('resources')->put('lang/' . $key . '.json', $lng);

        }

        $bar->finish();
    }

    private function getRoutes()
    {
        $routes = [];
        foreach (Route::getRoutes()->getRoutes() as $route) {
            if (isset($route->action["middleware"]) && is_array($route->action["middleware"]) && in_array('role',
                    $route->action["middleware"])) {
                $routes[] = $route;
            }
        }

        return $routes;
    }

    private function collectMenuTitles($menu, &$menuItems)
    {
        foreach ($menu as $m) {
            $menuItems[] = $m['title'];

            if (isset($m['submenu'])) {
                $this->collectMenuTitles($m['submenu'], $menuItems);
            }
        }
    }
}

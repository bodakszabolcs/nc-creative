<?php

return array(
    'payment_prefix' => env('PAYMENT_PREFIX', \Illuminate\Support\Str::slug(env('APP_NAME', 'Laravel'))),
    'payment_methods' => [
        'Simple/CCVISAMC' => 'SimplePay - Bankkártya',
        'Simple/WIRE' => 'SimplePay - Átutalás',
        'Barion' => 'Barion - Bankkártya',
        'OTP' => 'OTP - Bankkártya',
        'OTP-07' => 'SZÉP vendéglátás kártya',
        'OTP-08' => 'SZÉP szabadidő kártya',
        'OTP-09' => 'SZÉP szálláshely kártya',
    ]
);

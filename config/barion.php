<?php

return array(
    'sandbox' => env('BARION_SANDBOX', true),
    'version' => 2,
    'use_bundle_root' => false,
    'payee_email' => 'bodak.szabolcs+2@gmail.com', //kedvezményezett bolt barion email címe
    'bundle_root_certificates' => '/storage/barion/ssl',
    'sandbox_settings' => [
        'posKey' => env('BARION_SANDBOX_POS_KEY'),
        'env' => 'test'
    ],
    'live_settings' => [
        'posKey' => env('BARION_LIVE_POS_KEY'),
        'env' => 'prod'
    ]
);
